﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
		public static string jogadorUserName = "Nome do Jogador";
		public static string jogadorUserID;
		public static string jogadorUserPhotoURL;

		public static int friendsTotal;
		public static List<object> friendsList;
		public static List<object> friendsListOriginal;
		public static Dictionary<string, object> friendData;
		public static Dictionary<string, object> friendPictureData;

		//New game with friend
		public static string tempChallengedFriendID;
		public static string tempChallengedFriendName;
		public static string tempChallengedFriendPhotoURL;


		public static List<object> tempUserCards;
		public static List<object> tempFriendCards;

		public static List<object> tempUserHandCards;
		public static List<object> tempFriendHandCards;


		public static bool userLoseAllCards = true;
		public static bool userDrawWithFriend = false;
		public static bool userWatchedTheChallenge = false;
		public static bool challengeTime = true;

		public static string gameAppToken;
		//public static string mainURL = "https://duelo.quatrorodas.abril.com.br/";
		public static string mainAPPID = "476129192546108";	
		public static string mainAPPSecret = "4147d9fec32b4c715d5cfb33a70913ab";
		public static string mainURL = "http://clientes.cheny.com.br/edabril/quatrorodas/supertrunfo/";
		//public static string mainAPPID = "204465162922433";	
		//public static string mainAPPSecret = "97d544807539cd6d68ad228ba0a2f82c";
		



		public static bool isLocal = false;
		public static GameManager instance;

		public static GameManager Instance {
				get {
						if (instance == null) {
								var gB = new GameObject ("GameManager");
								instance = gB.AddComponent<GameManager> ();
						}
						return instance;
				}
		}

		public static void CreateLists(){
			tempUserCards = new List<object>();
			tempFriendCards = new List<object>();
			tempUserHandCards = new List<object>();
			tempFriendHandCards = new List<object>();
		}

		public static object GetFriendsNames (int index)
		{
				object friendName;
				if (friendsList != null && friendsList.Count > 0) {
						Dictionary<string, object> tempFriendData = friendsList [index] as Dictionary<string, object>;
						friendName = tempFriendData ["name"];
				} else {

						List<object> fakeNames = new List<object> ();
						fakeNames.Add ("Nome do amigo");
						fakeNames.Add ("João Mané");
						fakeNames.Add ("João José");
						fakeNames.Add ("João João");
						fakeNames.Add ("Cheny Schmeling");
						fakeNames.Add ("Chenay Schmeling");

						friendName = fakeNames [Random.Range (0, fakeNames.Count)];
				}
				return friendName;
		}

		public static object GetFriendsURL (int index)
		{
				object friendURL = null;
				if (friendsList != null && friendsList.Count > 0) {
						Dictionary<string, object> tempFriendData = friendsList [index] as Dictionary<string, object>;
						try {
								Dictionary<string, object> tempFriendPicture = tempFriendData ["picture"] as Dictionary<string, object>;
								Dictionary<string, object> tempFriendPictureData = tempFriendPicture ["data"] as Dictionary<string, object>;
								friendURL = tempFriendPictureData ["url"];

						} catch (System.Exception ex) {
								Debug.Log(ex);
								object friendID = tempFriendData ["id"];	
								friendURL = Util.GetPictureURL (friendID.ToString (), 128, 128);
						}
				} else {
						friendURL = "...";
				}

				return friendURL;
		}

		public static object GetFriendID (int index)
		{
				object friendID = null;
				if (friendsList != null && friendsList.Count > 0) {
						Dictionary<string, object> tempFriendData = friendsList [index] as Dictionary<string, object>;
						friendID = tempFriendData ["id"];	
				} else {
						friendID = 0;
				}

				return friendID;
		}

		public static void DownloadImage (string url, UITexture targetTexture)
		{   
				if (isLocal) {
						url = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/v/t1.0-1/p50x50/10155753_10152420965508690_7021049977340795871_n.jpg?oh=e4839c93519f373258f5820d9ad2131c&oe=55DFF95A&__gda__=1436493286_593fb02ca0c11dbbd1651680d9de6709";
				}
						
				char tempChar = url [0];

				if (tempChar.ToString () == "/") {
						FB.API (url, Facebook.HttpMethod.GET, result => {
								if (result.Error != null) {
										Util.LogError (result.Error);
										return;
								}
								string userURL = Util.DeserializePictureURLString (result.Text);
								GameManager.Instance.StartCoroutine (coDownloadImage (userURL, targetTexture));
						});
				} else {
						GameManager.Instance.StartCoroutine (coDownloadImage (url, targetTexture));
				}
		}

		static IEnumerator coDownloadImage (string imageUrl, UITexture targetTexture)
		{
				WWW www = new WWW (imageUrl);
				yield return www;
				targetTexture.mainTexture = new Texture2D (www.texture.width, www.texture.height, TextureFormat.DXT1, false);
				www.LoadImageIntoTexture (targetTexture.mainTexture as Texture2D);
				www.Dispose ();
				www = null;
		}
}
