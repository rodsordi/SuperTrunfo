﻿using UnityEngine;
using System.Collections;

public class CardContainer : MonoBehaviour
{

		public string cardNumber;
		public GameObject selectButton;
		public GameObject card;

		public bool isCardChosen;

		// Use this for initialization
		void Start ()
		{

				/*
				gameObject.AddComponent<TweenPosition> ();
				gameObject.GetComponent<TweenPosition> ().method = UITweener.Method.EaseOut;

				gameObject.GetComponent<TweenPosition> ().from = new Vector3 (transform.localPosition.x,
						transform.localPosition.y,
						transform.localPosition.z);

				gameObject.GetComponent<TweenPosition> ().to = new Vector3 (transform.localPosition.x,
						transform.localPosition.y,
						transform.localPosition.z);
						*/

				//NGUITools.SetActive (selectButton, false);
				//card.GetComponent<BoxCollider> ().enabled = false;

		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void CardSelectedToChange ()
		{
				selectButton.GetComponent<UIButton> ().isEnabled = false;
				selectButton.GetComponentInChildren<UISprite> ().spriteName = "CardChangeBG_Selected";

				MyTurnManager challengeFriendManager = GameObject.Find ("MyTurnManager").GetComponent<MyTurnManager> ();
				challengeFriendManager.ExchangeCards (this);
		}

		public void MakeItSelectable ()
		{
				selectButton.GetComponent<UIButton> ().isEnabled = true;
				selectButton.GetComponentInChildren<UISprite> ().spriteName = "BackgroundBlackMenu";
		}
				
		public void PlaySelectedAnimation ()
		{
				/*
				if (!isSelectingCategories) {
						TweenAlpha.Begin (categorySelected.transform.FindChild ("Sprite_Locked").gameObject, 0.3f, 1);
						TweenScale.Begin (categorySelected.transform.FindChild ("Sprite_Locked").gameObject, 0.3f, new Vector3 (1, 1, 1));
				} else {
						TweenAlpha.Begin (categorySelected.transform.FindChild ("Sprite_Locked").gameObject, 0, 0);
						TweenScale.Begin (categorySelected.transform.FindChild ("Sprite_Locked").gameObject, 0.3f, new Vector3 (20, 20, 1));
				}
				*/
		}
				
}
