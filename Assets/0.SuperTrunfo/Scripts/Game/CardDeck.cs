﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardDeck : MonoBehaviour
{
		public static List<string[]> deck = new List<string[]> ();
		public static List<string[]> singlePlayerDeck = new List<string[]> ();
		public static List<string[]> userDeck = new List<string[]> ();
		public static List<string[]> friendDeck = new List<string[]> ();

		public static int[] size = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33};

		/*
		04 - "Potência"
		05 - "Torque"
		06 - "Cons. urb"
		07 - "Ruído p.m"
		08 - "Aceleração"
		09 - "Fren. 120-0"
		10 - "Retom. 40-80"
		11 - "Porta-malas"
		*/

		// cardgroup, cardNumber, cardPhoto, category01 ~ category08
		public static string[] card1A = { "Jaguar F-Type V8 S", "A", "01", 					"01.jpg", "495", "63,7", "7,1",  "47,7", "4,2",  "57,3", "1,8", "200", "01","1" };
		public static string[] card2A = { "Mercedes-Benz E63 AMG", "A", "02", 				"02.jpg", "557", "73,4", "8,3",  "45,3", "4,0",  "54,7", "2,0", "540", "02","2"  };
		public static string[] card3A = { "Chevrolet Camaro SS V8 6.2", "A", "03", 			"03.jpg", "426", "58",   "6,1",  "39,9", "5,7",  "51,2", "4,1", "320", "03","3"  };
		public static string[] card4A = { "Mercedes-Benz SL 400", "A", "04", 				"04.jpg", "333", "48,5", "9,3",  "39,7", "5,4",  "62,1", "2,4", "381", "04","4"  };
		public static string[] card5A = { "Honda Civic Si", "A", "05", 						"05.jpg", "206", "23,9", "8,9",  "40,4", "8,2",  "65,9", "4,7", "449", "05","5"  };
		public static string[] card6A = { "BMW 435i Coupé M Sport", "A", "06", 				"06.jpg", "306", "40,8", "9,0",  "42,7", "5,0",  "61,9", "2,3", "445", "06","6"  };
		public static string[] card7A = { "Fiat 500 Abarth", "A", "07", 					"07.jpg", "167", "23",   "9,2",  "44,4", "9,0",  "69,3", "6,2", "185", "07","7"  };
		public static string[] card8A = { "Jaguar XFR-S", "A", "08", 						"08.jpg", "550", "69,4", "6,7",  "46,1", "4,7",  "57,4", "1,9", "500", "08","8"  };
		public static string[] card1B = { "Audi A8 4.0 TFSI Quattro", "B", "01",			"09.jpg", "435", "61,2", "6,6",  "34,2", "4,3",  "60,3", "2,2", "520", "09","9"  };
		public static string[] card2B = { "Ford Fusion Titanium Hybrid", "B", "02", 		"10.jpg", "145", "18",   "21,3", "71,2", "9,8",  "65,8", "4,4", "514", "10","10"  };
		public static string[] card3B = { "Mercedes C180 Avantgarde", "B", "03", 			"11.jpg", "156", "25,6", "12,6", "39,7", "9,6",  "62,9", "4,3", "475", "11","11"  };
		public static string[] card4B = { "Audi A3 1.8 Turbo Cabriolet", "B", "04", 		"12.jpg", "180", "25,7", "10,6", "40,2", "7,9",  "54,5", "3,4", "287", "12","12"  };
		public static string[] card5B = { "Volkswagen Golf Highline 1.4 DSG", "B", "05", 	"13.jpg", "140", "25,5", "11,5", "34",   "8,9",  "58,4", "4,9", "313", "13","13"  };
		public static string[] card6B = { "Suzuki Swift Sport R", "B", "06", 				"14.jpg", "143", "17",   "12,4", "39,1", "9,3",  "57,5", "5,4", "210", "14","14"  };
		public static string[] card7B = { "Citroën C4 Lounge THP Tendance", "B", "07", 		"15.jpg", "166", "24,5", "8,5",  "40,7", "9,2",  "64,2", "4,2", "450", "15","15"  };
		public static string[] card8B = { "Porsche Macan Turbo", "B", "08", 				"16.jpg", "400", "56,1", "10,3", "45,8", "4,6",  "59,9", "2,8", "505", "16","16"  };
		public static string[] card1C = { "Dodge Journey R/T AWD", "C", "01", 				"17.jpg", "280", "34,9", "6,8",  "39,8", "9,5",  "71,1", "5,1", "483", "17","17"  };
		public static string[] card2C = { "Renault Fluence Dynamique 2.0", "C", "02", 		"18.jpg", "141", "19,9", "9,6",  "41,3", "10,7", "65,6", "4,6", "530", "18","18"  };
		public static string[] card3C = { "Jeep Cherokee", "C", "03", 						"19.jpg", "271", "32,2", "7,9",  "44,9", "9,1",  "66,1", "4,8", "412", "19","19"  };
		public static string[] card4C = { "Hyundai Azera", "C", "04", 						"20.jpg", "250", "28,8", "7,7",  "36,9", "10,1", "65",   "5,0", "461", "20","20"  };
		public static string[] card5C = { "BMW X1", "C", "05", 								"21.jpg", "184", "27,5", "9,1",  "40,8", "7,6",  "63,2", "3,6", "420", "21","21"  };
		public static string[] card6C = { "Honda City EXL", "C", "06", 						"22.jpg", "115", "15,3", "12,5", "38,6", "11,7", "67,7", "5,7", "536", "22","22"  };
		public static string[] card7C = { "Chevrolet Cruze LTZ 1.8", "C", "07", 			"23.jpg", "142", "17,8", "11,1", "45,9", "11,6", "55,7", "5,2", "451", "23","23"  };
		public static string[] card8C = { "Toyota Corolla 2.0 XEi", "C", "08", 				"24.jpg", "144", "19,8", "10,9", "37",   "10,3", "67,5", "4,5", "470", "24","24"  };
		public static string[] card1D = { "Renault Logan 1.6 Easy´R", "D", "01", 			"25.jpg", "98",  "14,5", "10,7", "44,2", "16,8", "62,7", "7,5", "510", "25","25"  };
		public static string[] card2D = { "Chevrolet Spin Activ", "D", "02", 				"26.jpg", "106", "16,4", "10,0", "42,3", "13,4", "63,1", "5,8", "710", "26","26"  };
		public static string[] card3D = { "Hyundai HB20 1.0 Comfort", "D", "03", 			"27.jpg", "75",  "9,4",  "11,9", "41,2", "16,1", "72,2", "9,6", "300", "27","27"  };
		public static string[] card4D = { "Fiat Grand Siena Essence 1.6", "D", "04", 		"28.jpg", "116", "16,2", "10,4", "44,6", "11,4", "64,5", "8,4", "525", "28","28"  };
		public static string[] card5D = { "Nissan March S 1.0", "D", "05", 					"29.jpg", "74",  "10",   "11,8", "44,3", "14,5", "64,8", "9",   "265", "29","29"  };
		public static string[] card6D = { "Ford Ka+ 1.5 SEL", "D", "06", 					"30.jpg", "110", "14,9", "12,1", "46,8", "11,1", "60,9", "7,9", "446", "30","30"  };
		public static string[] card7D = { "Volkswagen Take Up! 1.0", "D", "07", 			"31.jpg", "76",  "10,4", "14,1", "38,8", "14,2", "70,1", "8,6", "285", "31","31"  };
		public static string[] card8D = { "Kia Soul 1.6 Flex", "D", "08", 					"32.jpg", "122", "16",   "10,2", "38,7", "14,4", "59,0", "5,9", "686", "32","32"  };
		public static string[] card1E = { "Porsche 911 turbo", "E", "01", 					"33.jpg", "560", "71,4", "7,5",  "55",   "3,1",  "52,5", "2,1", "260", "33","32"  };

		public static void ArrangeDeck(){

				//deck.Clear();

				deck.Add(card1A);
				deck.Add(card2A);
				deck.Add(card3A);
				deck.Add(card4A);
				deck.Add(card5A);
				deck.Add(card6A);
				deck.Add(card7A);
				deck.Add(card8A);
				deck.Add(card1B);
				deck.Add(card2B);
				deck.Add(card3B);
				deck.Add(card4B);
				deck.Add(card5B);
				deck.Add(card6B);
				deck.Add(card7B);
				deck.Add(card8B);
				deck.Add(card1C);
				deck.Add(card2C);
				deck.Add(card3C);
				deck.Add(card4C);
				deck.Add(card5C);
				deck.Add(card6C);
				deck.Add(card7C);
				deck.Add(card8C);
				deck.Add(card1D);
				deck.Add(card2D);
				deck.Add(card3D);
				deck.Add(card4D);
				deck.Add(card5D);
				deck.Add(card6D);
				deck.Add(card7D);
				//deck.Add(card8D);
				deck.Add(card1E);

				//print ("Deck salvo");

				//ShuffleDeck ();

				/*
				for (int i = 0; i < deck.Count; i++) {
						print (deck [i][12]);
				}
				*/
		}

		public static void ShuffleDeck(){
				ShuffleList (deck);
				//deck.RemoveAt (deck.Count-1);
				//print ("Deck shuffleado");
		}

	public static void ArrangeHalfDeck(){
		singlePlayerDeck.Add(card1A);
		singlePlayerDeck.Add(card2A);
		singlePlayerDeck.Add(card3A);
		singlePlayerDeck.Add(card4A);
		singlePlayerDeck.Add(card5A);
		singlePlayerDeck.Add(card6A);
		singlePlayerDeck.Add(card7A);
		singlePlayerDeck.Add(card8A);
		singlePlayerDeck.Add(card1B);
		singlePlayerDeck.Add(card2B);
		singlePlayerDeck.Add(card3B);
		singlePlayerDeck.Add(card4B);
		singlePlayerDeck.Add(card5B);
		singlePlayerDeck.Add(card6B);
		singlePlayerDeck.Add(card7B);
		singlePlayerDeck.Add(card8B);
		singlePlayerDeck.Add(card1C);
		singlePlayerDeck.Add(card2C);
		singlePlayerDeck.Add(card3C);
		singlePlayerDeck.Add(card4C);
		singlePlayerDeck.Add(card5C);
		singlePlayerDeck.Add(card6C);
		singlePlayerDeck.Add(card7C);
		singlePlayerDeck.Add(card8C);
		singlePlayerDeck.Add(card1D);
		singlePlayerDeck.Add(card2D);
		singlePlayerDeck.Add(card3D);
		singlePlayerDeck.Add(card4D);
		singlePlayerDeck.Add(card5D);
		singlePlayerDeck.Add(card6D);
		singlePlayerDeck.Add(card7D);
		//singlePlayerDeck.Add(card8D);
		singlePlayerDeck.Add(card1E);
		singlePlayerDeck.RemoveRange(0, 16);
	}

	public static void ShuffleHalfDeck(){
		ShuffleList (singlePlayerDeck);
		//deck.RemoveAt (deck.Count-1);
		//print ("Deck shuffleado");
	}

		/*
		public static void ShuffleArray (List<string[]> arr)
		{
				for (int i = arr.Count - 1; i > 0; i--) {
						int r = Random.Range (0, i + 1);
						List<string[]> tmp = arr [i];
						arr [i] = arr [r];
						arr [r] = tmp;
				}
		}*/

		public static void ShuffleList<T>(IList<T> ts) {
				int count = ts.Count;
				int last = count - 1;
				for (int i = 0; i < last; ++i) {
						var r = Random.Range(i, count);
						var tmp = ts[i];
						ts[i] = ts[r];
						ts[r] = tmp;
				}
		}
}
