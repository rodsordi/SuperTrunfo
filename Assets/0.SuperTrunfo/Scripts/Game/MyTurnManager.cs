﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyTurnManager : MonoBehaviour
{

		public CardContainer cardPrefab;
		public List<CardContainer> cardContainerList = new List<CardContainer> ();

		public GameObject selectedCategory01;
		public GameObject selectedCategory02;
		public GameObject selectedCategory03;
		public GameObject selectedCategory04;

		string categoryLabel01;
		string categoryLabel02;
		string categoryLabel03;
		string categoryLabel04;

		CardContainer firstCardToExchange;
		CardContainer secondCardToExchange;

		bool nextButtonEnabled;

		List<string> categoriesList = new List<string> ();

		Color32 fontColor = new Color32 (77, 77, 77, 255);

		void Awake(){
				CardDeck.ArrangeDeck ();	
				CardDeck.ShuffleDeck ();
		}

		// Use this for initialization
		void Start ()
		{
				Debug.Log ("[Super Trunfo]: Loading ChallengeFriendScene");
				CreateCards ();
		}

		// Update is called once per frame
		void Update ()
		{
				if (!nextButtonEnabled) {
						int cardsChosen = 0;
						for (int i = 0; i < cardContainerList.Count; i++) {
								if (cardContainerList [i].isCardChosen) {
										cardsChosen++;
								}
						}

						if (cardsChosen >= 4) {
								nextButtonEnabled = true;
								GameObject.Find ("Button_Proximo").GetComponent<UIButton> ().isEnabled = true;
						}
				}
		}

		void CreateCards ()
		{
				for (int i = 0; i < 4; i++) {
						Vector3 cardPrefabPosition = new Vector3 (-1200, 0, 0);
						Vector3 cardPrefabScale = new Vector3 (1, 1, 1);

						CardContainer myCardPrefab = Instantiate (cardPrefab, cardPrefabPosition, Quaternion.identity) as CardContainer;
						myCardPrefab.name = "CardContainer0" + (i + 1);

						myCardPrefab.transform.parent = GameObject.Find ("PanelGame").transform;
						myCardPrefab.transform.localScale = cardPrefabScale;
						//myCardPrefab.transform.localPosition = new Vector3 (-390f + (260 * i), -70f, 0f);
						cardContainerList.Add (myCardPrefab);

						myCardPrefab.GetComponent<TweenPosition> ().delay = i * 0.3f + 1;
						myCardPrefab.GetComponent<TweenPosition> ().duration = 1;
						myCardPrefab.GetComponent<TweenPosition> ().from = new Vector3 (-1200, 0, 0);
						myCardPrefab.GetComponent<TweenPosition> ().to = new Vector3 (-390f + (260 * i), -70f, 0f);
						myCardPrefab.GetComponent<TweenPosition> ().PlayForward ();

						int cardOnDeck = 0;
						string[] userCard = null;

						Partida partida = GameController.Partida;

						if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
								cardOnDeck = IntParseFast (partida.CartasJogador[i].ToString());
						} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
								cardOnDeck = IntParseFast (partida.CartasAdversario[i].ToString());
						}
								
						for (int j = 0; j < CardDeck.deck.Count; j++) {
								if (IntParseFast(CardDeck.deck [j] [13]) == cardOnDeck) {
										userCard = CardDeck.deck [j];
								}
						}

			Debug.Log(myCardPrefab);
			Debug.Log(myCardPrefab.cardNumber);
			Debug.Log(userCard);
			Debug.Log(userCard[13]);
						myCardPrefab.cardNumber = userCard[13];

						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Sprite_CardPhoto").GetComponent<UISprite>().spriteName = userCard[12];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardNumber").GetComponent<UILabel>().text = userCard[1]+userCard[2];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardName").GetComponent<UILabel>().text = userCard[0];

						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Potencia").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [4];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Torque").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [5];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Consumo").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [6];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Ruido").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [7];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Aceleracao").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [8];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Frenagem").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [9];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Retomada").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [10];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_PortaMalas").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [11];
				}

				CreateCategories ();
		}

		void CreateCategories ()
		{
				categoriesList.Add ("Potência");
				categoriesList.Add ("Torque");
				categoriesList.Add ("Cons. urb");
				categoriesList.Add ("Ruído p.m");
				categoriesList.Add ("Aceleração");
				categoriesList.Add ("Fren. 120-0");
				categoriesList.Add ("Retom. 40-80");
				categoriesList.Add ("Porta-malas");

				Partida partida = GameController.Partida;

				selectedCategory01.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = "???";
				selectedCategory02.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = "???";
				selectedCategory03.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = "???";
				selectedCategory04.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = "???";

				categoryLabel01 = categoriesList[partida.CategoriaCarta1 - 4];
				categoryLabel02 = categoriesList[partida.CategoriaCarta2 - 4];
				categoryLabel03 = categoriesList[partida.CategoriaCarta3 - 4];
				categoryLabel04 = categoriesList[partida.CategoriaCarta4 - 4];
				selectedCategory01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().text = categoryLabel01;
				selectedCategory02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().text = categoryLabel02;
				selectedCategory03.transform.FindChild ("Label_Category").GetComponent<UILabel> ().text = categoryLabel03;
				selectedCategory04.transform.FindChild ("Label_Category").GetComponent<UILabel> ().text = categoryLabel04;

				SelectCategories ();
		}

		void SelectCategories ()
		{
				for (int i = 0; i < cardContainerList.Count; i++) {
						for (int j = 0; j < cardContainerList [i].card.transform.childCount; j++) {
								if (cardContainerList [i].card.transform.GetChild (j).GetComponent<CardCategory> ()) {

										cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().spriteName = "CardSpecBG";
										cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
										cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Points").GetComponent<UILabel> ().color = Color.white;

										if (i == 0) {
												if (cardContainerList [i].card.transform.GetChild (j).GetComponent<CardCategory> ().categoryLabel == categoryLabel01) {
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().spriteName = "CardSpecBG_Selected";
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Points").GetComponent<UILabel> ().color = fontColor;
												}
										}

										if (i == 1) {
												if (cardContainerList [i].card.transform.GetChild (j).GetComponent<CardCategory> ().categoryLabel == categoryLabel02) {
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().spriteName = "CardSpecBG_Selected";
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Points").GetComponent<UILabel> ().color = fontColor;
												}
										}

										if (i == 2) {
												if (cardContainerList [i].card.transform.GetChild (j).GetComponent<CardCategory> ().categoryLabel == categoryLabel03) {
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().spriteName = "CardSpecBG_Selected";
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Points").GetComponent<UILabel> ().color = fontColor;
												}
										}

										if (i == 3) {
												if (cardContainerList [i].card.transform.GetChild (j).GetComponent<CardCategory> ().categoryLabel == categoryLabel04) {
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().spriteName = "CardSpecBG_Selected";
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
														cardContainerList [i].card.transform.GetChild (j).GetComponent<UISprite> ().transform.FindChild ("Label_Points").GetComponent<UILabel> ().color = fontColor;
												}
										}


								}
						}
				}
		}

		public void ExchangeCards (CardContainer cardContainer)
		{
				//Debug.Log (cardContainer.localPosition);

				if (firstCardToExchange == null) {
						firstCardToExchange = cardContainer;
				} else {
						secondCardToExchange = cardContainer;
				}

				if (firstCardToExchange && secondCardToExchange) {
						Vector3 firstCardPosition = new Vector3 (firstCardToExchange.transform.localPosition.x, 
								                            firstCardToExchange.transform.localPosition.y, 
								                            firstCardToExchange.transform.localPosition.z); 
						Vector3 secondCardPosition = new Vector3 (secondCardToExchange.transform.localPosition.x, 
								                             secondCardToExchange.transform.localPosition.y, 
								                             secondCardToExchange.transform.localPosition.z);

						firstCardToExchange.GetComponent<TweenPosition> ().from = firstCardPosition;
						firstCardToExchange.GetComponent<TweenPosition> ().to = secondCardPosition;
						firstCardToExchange.GetComponent<TweenPosition> ().delay = 0;

						secondCardToExchange.GetComponent<TweenPosition> ().from = secondCardPosition;
						secondCardToExchange.GetComponent<TweenPosition> ().to = firstCardPosition;
						//gameObject.GetComponent<TweenPosition> ().method = UITweener.Method.EaseOut;
						secondCardToExchange.GetComponent<TweenPosition> ().delay = 0;

						secondCardToExchange.GetComponent<TweenPosition> ().AddOnFinished (OnPositionFinished);


						TweenPosition.Begin (firstCardToExchange.gameObject, 0.5f, secondCardPosition);
						TweenPosition.Begin (secondCardToExchange.gameObject, 0.5f, firstCardPosition);


						firstCardToExchange.MakeItSelectable ();
						secondCardToExchange.MakeItSelectable ();

						int tmp = cardContainerList.IndexOf (firstCardToExchange);
						int tmp2 = cardContainerList.IndexOf (secondCardToExchange);

						cardContainerList [tmp] = secondCardToExchange;
						cardContainerList [tmp2] = firstCardToExchange;

						firstCardToExchange = null;
						secondCardToExchange = null;
				}
		}

		void OnPositionFinished ()
		{
				SelectCategories ();
		}

		public void NextScene ()
		{
				Partida partida = GameController.Partida;

				List<object> newTurnHand = new List<object> ();
				for (int i = 0; i < cardContainerList.Count; i++) {
						newTurnHand.Add (cardContainerList [i].cardNumber);
				}
						
				if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
						partida.MaoJogador = newTurnHand;
						partida.MaoJogadorAnterior = partida.MaoJogador;
						for (int j = 0; j < 4; j++) {
								partida.CartasJogador [j] = partida.MaoJogador [j];
						}

				} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
						partida.MaoAdversario = newTurnHand;
						partida.MaoAdversarioAnterior = partida.MaoAdversario;
						for (int j = 0; j < 4; j++) {
								partida.CartasAdversario [j] = partida.MaoAdversario [j];
						}
				}
						
				GameManager.challengeTime = true;
				Application.LoadLevel("06.CardBattleScene");
		}

		public void PreviousScene ()
		{
				Application.LoadLevel("02.GameMenuScene");
		}

		public static int IntParseFast (string value)
		{
				int result = 0;
				for (int i = 0; i < value.Length; i++) {
						char letter = value [i];
						result = 10 * result + (letter - 48);
				}
				return result;
		}

}
