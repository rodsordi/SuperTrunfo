﻿using UnityEngine;
using System.Collections;

public class CardBattleFinishedManager : MonoBehaviour
{

		//public class GameController : MonoBehaviour {
		public static GameController gameController;

		public static Jogador Jogador;
		public static Jogador Adversario;

		public GameObject card01;
		public GameObject card02;
		public GameObject card03;
		public GameObject card04;
		public UILabel title;
		public UILabel title2;

	
		public static GameController GetInstance ()
		{

				if (gameController == null) {
						Instantiate (Resources.Load ("GameController"));
				}
				return gameController;
		}

		public void Start ()
		{
				AudioManager.Instance.CheckSceneAndUpdate();
				if (GameManager.userWatchedTheChallenge && title) {
						title2.text = "O jogador adversário já escolheu as categorias para batalhar.";
						title.text = "Agora é a sua vez!";
						return;
				}

				if (GameManager.userDrawWithFriend && title) {
						title.text = "Empate! Ninguém perdeu cartas, vamos para a próxima rodada?";
						return;
				}

				if (GameManager.userLoseAllCards && title) {
						title.text = "Que pena, você perdeu essa rodada, vamos para a próxima?";
				}

				GameManager.userDrawWithFriend = false;
				GameManager.userLoseAllCards = false;
		}

		public void NextScene ()
		{
				if (GameManager.userWatchedTheChallenge) {
						GameManager.userWatchedTheChallenge = false;
						Application.LoadLevel ("05.MyTurnScene");
				} else {

						Application.LoadLevel ("03.NewGameScene");
				}
		}

		public void Share ()
		{
				FB.Feed (
						linkCaption: "Eu ganhei 04 cartas no Desafio Quatro Rodas!",
						picture: GameManager.mainURL + "Ganhei04Cartas.png",
						linkName: "Duelo Quatro Rodas",
						link: "http://apps.facebook.com/" + FB.AppId + "/?challenge_brag=" + (FB.IsLoggedIn ? FB.UserId : "guest")
				);
		}

		public void SkipShare ()
		{
				Partida partida = GameController.Partida;

				if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
						if (partida.CartasJogador.Count <= 0) {
								Application.LoadLevel ("09.SinglePlayerSceneGameOver");
								return;
						}
						if (partida.CartasAdversario.Count <= 0) {
								Application.LoadLevel ("09.SinglePlayerSceneGameWin");
								return;
						}
				} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
						if (partida.CartasAdversario.Count <= 0) {
								Application.LoadLevel ("09.SinglePlayerSceneGameOver");
								return;
						}
						if (partida.CartasJogador.Count <= 0) {
								Application.LoadLevel ("09.SinglePlayerSceneGameWin");
								return;
						}
				}




						

				Application.LoadLevel ("07.CardBattleFinishedScene");
		}
}
