﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardBattleManager : MonoBehaviour
{

		public GameObject feedBackContainer;
		public GameObject categoryPlayer01;
		public GameObject categoryPlayer02;
		public GameObject battlePanel;
		//public UILabel roundLabel;
		public UILabel titleLabel;
		public UILabel cardsLabel;
		public UILabel feedbackLabel;
		public UISprite feedbackSprite;
		public CardContainer jogadorCard;
		public CardContainer adversarioCard;
		string labelWinner = "BOA JOGADA! ESSA CARTA É SUA!";
		string labelLoser = "OPA! PERDEU UMA CARTA!";
		List<float> categoriesPositions = new List<float> ();
		int currentRound = 0;
		int myScore;
		int enemyScore;
		Color32 fontColor = new Color32 (139, 135, 136, 255);
		string[] categories;
		public UILabel userName;
		public UILabel friendName;
		public UILabel userScore;
		public UILabel friendScore;
		public UITexture userPhoto;
		public UITexture friendPhoto;
		public UISprite typeModeSprite;
		public UIWidget cardPlayer01;
		public UIWidget cardPlayer02;

		//FAKE
		List<int> categoriesSelected = new List<int> ();
		List<string> myCategoriesPoints = new List<string> ();
		List<string> enemyCategoriesPoints = new List<string> ();

		void Awake ()
		{	
				CardDeck.ArrangeDeck ();
				CardDeck.ShuffleDeck ();
		}
				
		// Use this for initialization
		void Start ()
		{
				AudioManager.Instance.CheckSceneAndUpdate ();
				categoriesPositions.Add (25);
				categoriesPositions.Add (-10);
				categoriesPositions.Add (-45);
				categoriesPositions.Add (-80);
				categoriesPositions.Add (-115);
				categoriesPositions.Add (-150);
				categoriesPositions.Add (-185);
				categoriesPositions.Add (-220);

				/*
				CreateFakeChooses ();
				NewCardIntoGame ();
				NewFeedbackPosition ();
				CheckWhosWinner ();
				*/
				GameObject painel = GameObject.Find ("Panel_Battle");
				NGUITools.SetActive (painel, false);

				Partida partida = GameController.Partida;
				if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
						userName.text = partida.Jogador.Nome;
						friendName.text = partida.Adversario.Nome;
						userScore.text = partida.CartasJogador.Count.ToString ();
						friendScore.text = partida.CartasAdversario.Count.ToString ();

						GameManager.DownloadImage (partida.Jogador.FotoURL, userPhoto);
						GameManager.DownloadImage (partida.Adversario.FotoURL, friendPhoto);

				} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
						userName.text = partida.Adversario.Nome;
						friendName.text = partida.Jogador.Nome;
						userScore.text = partida.CartasAdversario.Count.ToString ();
						friendScore.text = partida.CartasJogador.Count.ToString ();

						GameManager.DownloadImage (partida.Adversario.FotoURL, userPhoto);
						GameManager.DownloadImage (partida.Jogador.FotoURL, friendPhoto);
				}
				
				
						
				CreateChooses ();
				NewCardIntoGame ();

		}
	
		// Update is called once per frame

		float timeLeft = 2f;
		bool entrou;

		void Update ()
		{

				timeLeft -= Time.deltaTime;
				if (timeLeft < 0 && !entrou) {
						entrou = true;
						FreeBattlePanel ();
				}
		}

		void FreeBattlePanel ()
		{
				NGUITools.SetActive (battlePanel, true);
				//CreateChooses ();
				//NewCardIntoGame ();
				NewFeedbackPosition ();
				CheckWhosWinner ();
		}

		void CreateChooses ()
		{
				Partida partida = GameController.Partida;

				categories = new string[] {
						"Potência",
						"Torque",
						"Cons. urb",
						"Ruído p.m",
						"Aceleração",
						"Fren. 120-0",
						"Retom. 40-80",
						"Porta-malas"
				};
				//string[] myPoints = { "100", "200", "300", "400" };
				//string[] enemyPoints = { "50", "150", "290", "420" };

				Debug.Log (GameManager.challengeTime);


				if (GameManager.challengeTime) {
						typeModeSprite.spriteName = "ChallengeTime";
						if (partida.Rodada == 1)
								titleLabel.text = "PRIMEIRA RODADA";
						if (partida.Rodada == 2)
								titleLabel.text = "SEGUNDA RODADA";
						if (partida.Rodada == 3)
								titleLabel.text = "TERCEIRA RODADA";
						if (partida.Rodada == 4)
								titleLabel.text = "QUARTA RODADA";
			
				} else {
						typeModeSprite.spriteName = "ObserverTime";

						if (partida.Rodada == 2)
								titleLabel.text = "PRIMEIRA RODADA";
						if (partida.Rodada == 3)
								titleLabel.text = "SEGUNDA RODADA";
						if (partida.Rodada == 4)
								titleLabel.text = "TERCEIRA RODADA";
						if (partida.Rodada == 5)
								titleLabel.text = "QUARTA RODADA";
				}
		
				if (partida.StatusPartida == Partida.STATUS_JOGANDO && partida.Turno == Partida.TURNO_JOGADOR && partida.MaoJogador == null && partida.MaoAdversario != null) {
						categoriesSelected.Add (partida.CategoriaOld1);
						categoriesSelected.Add (partida.CategoriaOld2);
						categoriesSelected.Add (partida.CategoriaOld3);
						categoriesSelected.Add (partida.CategoriaOld4);
				} else if (partida.StatusPartida == Partida.STATUS_JOGANDO && partida.Turno == Partida.TURNO_ADVERSARIO && partida.MaoAdversario == null && partida.MaoJogador != null) {
						categoriesSelected.Add (partida.CategoriaOld1);
						categoriesSelected.Add (partida.CategoriaOld2);
						categoriesSelected.Add (partida.CategoriaOld3);
						categoriesSelected.Add (partida.CategoriaOld4);
				} else if (partida.StatusPartida == Partida.STATUS_FECHADO) {
						categoriesSelected.Add (partida.CategoriaOld1);
						categoriesSelected.Add (partida.CategoriaOld2);
						categoriesSelected.Add (partida.CategoriaOld3);
						categoriesSelected.Add (partida.CategoriaOld4);
				} else {
						categoriesSelected.Add (partida.CategoriaCarta1);
						categoriesSelected.Add (partida.CategoriaCarta2);
						categoriesSelected.Add (partida.CategoriaCarta3);
						categoriesSelected.Add (partida.CategoriaCarta4);
				}

				/*
				myCategoriesPoints.Add (partida.MaoJogadorAnterior [0].ToString());
				myCategoriesPoints.Add (partida.MaoJogadorAnterior [1].ToString());
				myCategoriesPoints.Add (partida.MaoJogadorAnterior [2].ToString());
				myCategoriesPoints.Add (partida.MaoJogadorAnterior [3].ToString());

				enemyCategoriesPoints.Add (partida.MaoAdversarioAnterior [0].ToString());
				enemyCategoriesPoints.Add (partida.MaoAdversarioAnterior [1].ToString());
				enemyCategoriesPoints.Add (partida.MaoAdversarioAnterior [2].ToString());
				enemyCategoriesPoints.Add (partida.MaoAdversarioAnterior [3].ToString());

				ShuffleArray (categories);
				ShuffleArray (myPoints);
				ShuffleArray (enemyPoints);
				*/

				//categoriesSelected.AddRange (categories);
				//myCategoriesPoints.AddRange (myPoints);
				//enemyCategoriesPoints.AddRange (enemyPoints);
		}

		void NewCardIntoGame ()
		{
				Partida partida = GameController.Partida;

				int userCardOnDeck = 0;
				int friendCardOnDeck = 0;
				string[] userCard = null;
				string[] friendCard = null;

				if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
						userCardOnDeck = IntParseFast (partida.MaoJogadorAnterior [currentRound].ToString ());
						friendCardOnDeck = IntParseFast (partida.MaoAdversarioAnterior [currentRound].ToString ());

						if (partida.MaoJogador != null && partida.MaoAdversario != null) {
								userCardOnDeck = IntParseFast (partida.MaoJogador [currentRound].ToString ());
								friendCardOnDeck = IntParseFast (partida.MaoAdversario [currentRound].ToString ());
						}

				} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
						userCardOnDeck = IntParseFast (partida.MaoAdversarioAnterior [currentRound].ToString ());
						friendCardOnDeck = IntParseFast (partida.MaoJogadorAnterior [currentRound].ToString ());
				}

				for (int j = 0; j < CardDeck.deck.Count; j++) {
						if (IntParseFast (CardDeck.deck [j] [13]) == userCardOnDeck) {
								userCard = CardDeck.deck [j];
						}

						if (IntParseFast (CardDeck.deck [j] [13]) == friendCardOnDeck) {
								friendCard = CardDeck.deck [j];
						}
				}
						
				jogadorCard.cardNumber = userCard [13];

				jogadorCard.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = userCard [12];
				jogadorCard.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = userCard [1] + userCard [2];
				jogadorCard.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardName").GetComponent<UILabel> ().text = userCard [0];

				jogadorCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Potencia").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [4];
				jogadorCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Torque").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [5];
				jogadorCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Consumo").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [6];
				jogadorCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Ruido").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [7];
				jogadorCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Aceleracao").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [8];
				jogadorCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Frenagem").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [9];
				jogadorCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Retomada").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [10];
				jogadorCard.transform.FindChild ("Card").FindChild ("Sprite_Category_PortaMalas").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [11];

				adversarioCard.cardNumber = friendCard [13];

				adversarioCard.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = friendCard [12];
				adversarioCard.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = friendCard [1] + friendCard [2];
				adversarioCard.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardName").GetComponent<UILabel> ().text = friendCard [0];

				adversarioCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Potencia").FindChild ("Label_Points").GetComponent<UILabel> ().text = friendCard [4];
				adversarioCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Torque").FindChild ("Label_Points").GetComponent<UILabel> ().text = friendCard [5];
				adversarioCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Consumo").FindChild ("Label_Points").GetComponent<UILabel> ().text = friendCard [6];
				adversarioCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Ruido").FindChild ("Label_Points").GetComponent<UILabel> ().text = friendCard [7];
				adversarioCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Aceleracao").FindChild ("Label_Points").GetComponent<UILabel> ().text = friendCard [8];
				adversarioCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Frenagem").FindChild ("Label_Points").GetComponent<UILabel> ().text = friendCard [9];
				adversarioCard.transform.FindChild ("Card").FindChild ("Sprite_Category_Retomada").FindChild ("Label_Points").GetComponent<UILabel> ().text = friendCard [10];
				adversarioCard.transform.FindChild ("Card").FindChild ("Sprite_Category_PortaMalas").FindChild ("Label_Points").GetComponent<UILabel> ().text = friendCard [11];

				myCategoriesPoints.Add (userCard [categoriesSelected [currentRound]]);
				enemyCategoriesPoints.Add (friendCard [categoriesSelected [currentRound]]);

				categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().text = categories [categoriesSelected [currentRound] - 4];
				categoryPlayer01.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = myCategoriesPoints [currentRound];

				categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().text = categories [categoriesSelected [currentRound] - 4];
				categoryPlayer02.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = enemyCategoriesPoints [currentRound];

				cardPlayer01.transform.FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = userCard [12];
				cardPlayer01.transform.FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = userCard [1] + userCard [2];
				cardPlayer01.transform.FindChild ("Label_CardName").GetComponent<UILabel> ().text = userCard [0];
					
				cardPlayer02.transform.FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = friendCard [12];
				cardPlayer02.transform.FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = friendCard [1] + friendCard [2];
				cardPlayer02.transform.FindChild ("Label_CardName").GetComponent<UILabel> ().text = friendCard [0];

				if (currentRound == 1) {
						NGUITools.SetActive (GameObject.Find ("Sprite_MyCard04"), false);
						NGUITools.SetActive (GameObject.Find ("Sprite_FriendCard04"), false);
				}
				if (currentRound == 2) {
						NGUITools.SetActive (GameObject.Find ("Sprite_MyCard03"), false);
						NGUITools.SetActive (GameObject.Find ("Sprite_FriendCard03"), false);
				}
				if (currentRound == 3) {
						NGUITools.SetActive (GameObject.Find ("Sprite_MyCard02"), false);
						NGUITools.SetActive (GameObject.Find ("Sprite_FriendCard02"), false);
				}
		}

		void NewFeedbackPosition ()
		{
				Vector3 newContainerPosition = new Vector3 (0, 0, 0);

				if (categories [categoriesSelected [currentRound] - 4] == "Potência") {
						newContainerPosition = new Vector3 (0, categoriesPositions [0], 0);
				}
				if (categories [categoriesSelected [currentRound] - 4] == "Torque") {
						newContainerPosition = new Vector3 (0, categoriesPositions [1], 0);
				}
				if (categories [categoriesSelected [currentRound] - 4] == "Cons. urb") {
						newContainerPosition = new Vector3 (0, categoriesPositions [2], 0);
				}
				if (categories [categoriesSelected [currentRound] - 4] == "Ruído p.m") {
						newContainerPosition = new Vector3 (0, categoriesPositions [3], 0);
				}
				if (categories [categoriesSelected [currentRound] - 4] == "Aceleração") {
						newContainerPosition = new Vector3 (0, categoriesPositions [4], 0);
				}
				if (categories [categoriesSelected [currentRound] - 4] == "Fren. 120-0") {
						newContainerPosition = new Vector3 (0, categoriesPositions [5], 0);
				}
				if (categories [categoriesSelected [currentRound] - 4] == "Retom. 40-80") {
						newContainerPosition = new Vector3 (0, categoriesPositions [6], 0);
				}
				if (categories [categoriesSelected [currentRound] - 4] == "Porta-malas") {
						newContainerPosition = new Vector3 (0, categoriesPositions [7], 0);
				}
						
				//feedBackContainer.transform.localPosition = newContainerPosition;
		}

		void CheckWhosWinner ()
		{
				//Ganha aquele que tem mais
				if (categories [categoriesSelected [currentRound] - 4] == "Potência" ||
						categories [categoriesSelected [currentRound] - 4] == "Torque" ||
						categories [categoriesSelected [currentRound] - 4] == "Cons. urb" ||
						categories [categoriesSelected [currentRound] - 4] == "Porta-malas") {

						if (float.Parse (myCategoriesPoints [currentRound], System.Globalization.CultureInfo.GetCultureInfo ("pt-BR").NumberFormat) > float.Parse (enemyCategoriesPoints [currentRound], System.Globalization.CultureInfo.GetCultureInfo ("pt-BR").NumberFormat)) {
								//Player01 Wins
								categoryPlayer01.GetComponent<UISprite> ().spriteName = "CategoryWinner";
								categoryPlayer02.GetComponent<UISprite> ().spriteName = "CategoryLoser";
				
								categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
								categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
				
								feedbackLabel.text = labelWinner;
								feedbackSprite.spriteName = "Shining";
								myScore++;
						} else {
								//Player02 Wins
								categoryPlayer01.GetComponent<UISprite> ().spriteName = "CategoryLoser";
								categoryPlayer02.GetComponent<UISprite> ().spriteName = "CategoryWinner";
				
								categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
								categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
				
								feedbackLabel.text = labelLoser;
								feedbackSprite.spriteName = "Shining_Loser";
								enemyScore++;
						}

				}
				

				//Ganha aquele que tem menos
				if (categories [categoriesSelected [currentRound] - 4] == "Ruído p.m" ||
						categories [categoriesSelected [currentRound] - 4] == "Aceleração" ||
						categories [categoriesSelected [currentRound] - 4] == "Fren. 120-0" ||
						categories [categoriesSelected [currentRound] - 4] == "Retom. 40-80") {

						if (float.Parse (myCategoriesPoints [currentRound], System.Globalization.CultureInfo.GetCultureInfo ("pt-BR").NumberFormat) < float.Parse (enemyCategoriesPoints [currentRound], System.Globalization.CultureInfo.GetCultureInfo ("pt-BR").NumberFormat)) {
								//Player01 Wins
								categoryPlayer01.GetComponent<UISprite> ().spriteName = "CategoryWinner";
								categoryPlayer02.GetComponent<UISprite> ().spriteName = "CategoryLoser";
				
								categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
								categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
				
								feedbackLabel.text = labelWinner;
								feedbackSprite.spriteName = "Shining";
								myScore++;
						} else {
								//Player02 Wins
								categoryPlayer01.GetComponent<UISprite> ().spriteName = "CategoryLoser";
								categoryPlayer02.GetComponent<UISprite> ().spriteName = "CategoryWinner";
				
								categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
								categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
				
								feedbackLabel.text = labelLoser;
								feedbackSprite.spriteName = "Shining_Loser";
								enemyScore++;
						}

				}
				



				feedBackContainer.GetComponent<iTweenEvent> ().Play ();

				Debug.Log ("User: " + myScore + " vs. " + enemyScore + " :Friend");
		}

		public void NextCard ()
		{
				currentRound++;
				if (currentRound >= 4) {
						//NextScene ();
						UpdatePartida ();
						return;
				}

				//roundLabel.text = "
				cardsLabel.text = (currentRound + 1) + "/4 cartas";

				NewCardIntoGame ();
				NewFeedbackPosition ();
				CheckWhosWinner ();
		}

		private void UpdatePartida ()
		{
				Partida partida = GameController.Partida;

				LoadingSystem.Instance.ShowLoading ();

				if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
						if (partida.StatusPartida == Partida.STATUS_JOGANDO) {
								if (partida.Turno == Partida.TURNO_JOGADOR) {
										if (partida.MaoJogador == null) {
												GameManager.userWatchedTheChallenge = true;

												List<object> tempList = new List<object> ();
												List<object> tempList2 = new List<object> ();

												for (int i = 0; i < 4; i++) {
														tempList.Add (partida.CartasJogador [i]);
														tempList2.Add (partida.CartasAdversario [i]);
												}
														
												partida.CategoriaOld1 = partida.CategoriaCarta1;
												partida.CategoriaOld2 = partida.CategoriaCarta2;
												partida.CategoriaOld3 = partida.CategoriaCarta3;
												partida.CategoriaOld4 = partida.CategoriaCarta4;
												partida.MaoJogador = tempList;
												partida.MaoJogadorAnterior = tempList;
												partida.MaoAdversarioAnterior = tempList2;
												partida.DatePartida = System.DateTime.Now;
												//partida.Rodada += 1;
												StartCoroutine (
														new PartidaDAO ().SaveOrUpdate (partida, () => {
														print ("Atualizei partida por aqui1");

														NextScene ();
												}));
												return;
										}
								}
						}
				}

				if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
						if (partida.StatusPartida == Partida.STATUS_JOGANDO) {
								if (partida.Turno == Partida.TURNO_ADVERSARIO) {
										if (partida.MaoAdversario == null) {

												GameManager.userWatchedTheChallenge = true;

												List<object> tempList = new List<object> ();
												List<object> tempList2 = new List<object> ();

												for (int i = 0; i < 4; i++) {
														tempList.Add (partida.CartasAdversario [i]);
														tempList2.Add (partida.CartasJogador [i]);
												}
												partida.CategoriaOld1 = partida.CategoriaCarta1;
												partida.CategoriaOld2 = partida.CategoriaCarta2;
												partida.CategoriaOld3 = partida.CategoriaCarta3;
												partida.CategoriaOld4 = partida.CategoriaCarta4;
												partida.MaoAdversario = tempList;
												partida.MaoAdversarioAnterior = tempList;
												partida.MaoJogadorAnterior = tempList2;
												partida.DatePartida = System.DateTime.Now;
												//partida.Rodada += 1;
												StartCoroutine (
														new PartidaDAO ().SaveOrUpdate (partida, () => {
														print ("Atualizei partida por aqui2");
														NextScene ();
												}));
												return;
										}
								}
						}
				}










				if (partida.StatusPartida != Partida.STATUS_FECHADO) {
						if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
								if (myScore > enemyScore) {
										for (int i = 0; i < 4; i++) {
												partida.CartasJogador.Add (partida.MaoAdversario [i]);
												partida.CartasJogador.Add (partida.MaoJogador [i]);
										}
								} else if (myScore < enemyScore) {
										for (int i = 0; i < 4; i++) {
												partida.CartasAdversario.Add (partida.MaoJogador [i]);
												partida.CartasAdversario.Add (partida.MaoAdversario [i]);
										}
								} else {
										for (int i = 0; i < 4; i++) {
												partida.CartasJogador.Add (partida.MaoJogador [i]);
												partida.CartasAdversario.Add (partida.MaoAdversario [i]);
										}
								}
						} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
								if (myScore > enemyScore) {
										for (int i = 0; i < 4; i++) {
												partida.CartasAdversario.Add (partida.MaoAdversario [i]);
												partida.CartasAdversario.Add (partida.MaoJogador [i]);
										}
								} else if (myScore < enemyScore) {
										for (int i = 0; i < 4; i++) {
												partida.CartasJogador.Add (partida.CartasJogador [i]);
												partida.CartasJogador.Add (partida.CartasAdversario [i]);
										}
								} else {
										for (int i = 0; i < 4; i++) {
												partida.CartasJogador.Add (partida.MaoJogador [i]);
												partida.CartasAdversario.Add (partida.MaoAdversario [i]);
										}
								}
						}				


						partida.CartasJogador.RemoveRange (0, 4);
						partida.CartasAdversario.RemoveRange (0, 4);
						partida.MaoJogador = null;
						partida.MaoAdversario = null;
						partida.StatusPartida = Partida.STATUS_JOGANDO;
						partida.DatePartida = System.DateTime.Now;
						partida.Rodada += 1;
						StartCoroutine (
								new PartidaDAO ().SaveOrUpdate (partida, () => {
								NextScene ();
						}));
				} else {
						NextScene ();
				}
		}

		void NextScene ()
		{
				Partida partida = GameController.Partida;
				

				if (myScore == enemyScore) {
						GameManager.userLoseAllCards = true;
						GameManager.userDrawWithFriend = true;
				}

				if (partida.StatusPartida == Partida.STATUS_FECHADO) {
						StartCoroutine (
								new PartidaDAO ().Delete (partida, () => {
								if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
										if (partida.CartasAdversario.Count <= 0) {
												GameController.Partidas.Remove (partida);
												GameController.Partida = null;
												Application.LoadLevel ("09.SinglePlayerSceneGameWin");
												return;
										}

										if (partida.CartasJogador.Count <= 0) {
												GameController.Partidas.Remove (partida);
												GameController.Partida = null;
												Application.LoadLevel ("09.SinglePlayerSceneGameOver");
												return;
										}

								} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
										if (partida.CartasJogador.Count <= 0) {
												GameController.Partidas.Remove (partida);
												GameController.Partida = null;
												Application.LoadLevel ("09.SinglePlayerSceneGameWin");
												return;
										}

										if (partida.CartasAdversario.Count <= 0) {
												GameController.Partidas.Remove (partida);
												GameController.Partida = null;
												Application.LoadLevel ("09.SinglePlayerSceneGameOver");
												return;
										}
								}


						}));
				} else {
						if (partida.CartasAdversario.Count <= 0 || partida.Rodada == 5) {
								ClosePartida (partida);
								return;
						}

						if (partida.CartasJogador.Count <= 0 || partida.Rodada == 5) {
								ClosePartida (partida);
								return;
						}

						if (myScore > enemyScore) {
								Application.LoadLevel ("07.CardBattleFinishedWinScene");
								return;
						} else {
								Application.LoadLevel ("07.CardBattleFinishedScene");
								return;
						}
				}
		}

		void ClosePartida (Partida partida)
		{
				partida.StatusPartida = Partida.STATUS_FECHADO;
				partida.DatePartida = System.DateTime.Now;

				StartCoroutine (
						new PartidaDAO ().SaveOrUpdate (partida, () => {
						//if (partida.CartasAdversario.Count <= 0) {
						if (partida.CartasAdversario.Count < partida.CartasJogador.Count) {
								Application.LoadLevel ("09.SinglePlayerSceneGameWin");
						} else

						//if (partida.CartasJogador.Count <= 0) {
						if (partida.CartasJogador.Count < partida.CartasAdversario.Count) {
								Application.LoadLevel ("09.SinglePlayerSceneGameOver");
						} else {
								Debug.Log ("Draw game");
								Application.LoadLevel ("09.SinglePlayerSceneGameDraw");
						}

						
				}));
				return;
		}

		public static void ShuffleArray<T> (T[] arr)
		{
				for (int i = arr.Length - 1; i > 0; i--) {
						int r = Random.Range (0, i + 1);
						T tmp = arr [i];
						arr [i] = arr [r];
						arr [r] = tmp;
				}
		}

		public static int IntParseFast (string value)
		{
				int result = 0;
				for (int i = 0; i < value.Length; i++) {
						char letter = value [i];
						result = 10 * result + (letter - 48);
				}
				return result;
		}
			
}