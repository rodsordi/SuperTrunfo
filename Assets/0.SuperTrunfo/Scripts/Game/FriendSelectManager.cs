﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FriendSelectManager : MonoBehaviour
{

		public GameObject gameMenuManager;
		public GameObject friendSelectPrefab;
		public GameObject friendSelectPanel;
		public UILabel searchLabelFeedback;
		private List<string> friendNamesList = new List<string> ();
		private List<string> friendNamesListFound = new List<string> ();
		private List<GameObject> friendPrefabList = new List<GameObject> ();
		private List<GameObject> friendPrefabFoundList = new List<GameObject> ();
		public UIPanel springPanel;

		int totalFriends;

		// Use this for initialization
		void Start ()
		{
				searchLabelFeedback.enabled = false;
				//PopulateWithFriends ();
		}

		// Update is called once per frame
		void Update ()
		{

		}

		public void OpenFriendSelect ()
		{
				Vector3 openPosition = new Vector3 (0, 0, 0);
				friendSelectPanel.transform.localPosition = openPosition;

				openPosition = new Vector3 (1500, 0, 0);
				gameMenuManager.transform.localPosition = openPosition;
		}

		public void CloseFriendSelect ()
		{
				Vector3 closePosition = new Vector3 (1500, 0, 0);
				friendSelectPanel.transform.localPosition = closePosition;

				closePosition = new Vector3 (0, 0, 0);
				gameMenuManager.transform.localPosition = closePosition;
		}

		public void PopulateWithFriends ()
		{
				int prefabInstantiated = 0;

				if (GameManager.isLocal) {
						totalFriends = 30;
				} else {
						totalFriends = GameManager.friendsList.Count;
				}

				for (int i = 0; i < totalFriends; i++) {
						Vector3 friendPreFabPosition = new Vector3 (0f, -25f - (40f * i), 0f);
						Vector3 friendPreFabScale = new Vector3 (1, 1, 1);

						GameObject myNewFriendPrefab = Instantiate (friendSelectPrefab, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;

						myNewFriendPrefab.transform.parent = GameObject.Find ("Panel_UserFriendsSelect").transform;
						myNewFriendPrefab.transform.localScale = friendPreFabScale;
						myNewFriendPrefab.transform.localPosition = friendPreFabPosition;

						//myNewFriendPrefab.GetComponentInChildren<UIButton> ().onClick.Add (new EventDelegate (this, "onFriendSelect"));
				
						string friendName;

						if (GameManager.isLocal) {
								friendName = GameManager.GetFriendsNames (i).ToString () + " " + i;
						} else {
								friendName = GameManager.GetFriendsNames (i).ToString ();
						}


						friendNamesList.Add (friendName);
						friendPrefabList.Add (myNewFriendPrefab);

						myNewFriendPrefab.GetComponentInChildren<UILabel> ().text = friendName;
						GameManager.DownloadImage (GameManager.GetFriendsURL (i).ToString (), myNewFriendPrefab.GetComponentInChildren<UITexture> ());

						UIButton btn = myNewFriendPrefab.GetComponentInChildren<UIButton> ();
						EventDelegate del = new EventDelegate (this, "onFriendSelect");
						del.parameters [0].value = friendName;
						del.parameters [1].value = GameManager.GetFriendsURL (i).ToString ();
						del.parameters [2].value = GameManager.GetFriendID (i).ToString ();
						EventDelegate.Set (btn.onClick, del);

						prefabInstantiated++;

						if (prefabInstantiated >= 2) {
								prefabInstantiated = 0;

								friendPreFabPosition = new Vector3 (360f, -25f - (40f * (i - 1)), 0f);
								myNewFriendPrefab.transform.localPosition = friendPreFabPosition;
						}
				}
		}

		public void SearchFriends ()
		{
				string searchString = GameObject.Find ("Label_SearchFriendName").GetComponent<UILabel> ().text;

				if (searchString == "") {
						ResetPosition ();
						return;
				}

				int matches = 0;
				int encontrados = 0;
				int prefabInstantiated = 0;
				friendNamesListFound.Clear ();
				friendPrefabFoundList.Clear ();

				for (int i = 0; i < friendNamesList.Count; i++) {
						matches = 0;
						for (int j = 0; j < searchString.Length; j++) {
								if (j < friendNamesList [i].Length) {
										if (searchString [j].ToString ().ToUpper () == friendNamesList [i] [j].ToString ().ToUpper ()) {
												matches += 1;
												if (matches == searchString.Length) {
														encontrados++;
														friendNamesListFound.Add (friendNamesList [i]);
												}
										}
								}
						}
				}

				print ("Encontrei " + encontrados);
				if (encontrados == 0) {
						searchLabelFeedback.enabled = true;
				} else {
						searchLabelFeedback.enabled = false;
				}
						
				for (int k = 0; k < friendPrefabList.Count; k++) {

						Vector3 newVector3 = new Vector3 (1000, 0, 0);
						friendPrefabList [k].transform.localPosition = newVector3;

						for (int l = 0; l < friendNamesListFound.Count; l++) {
								if (friendPrefabList [k].GetComponentInChildren<UILabel> ().text == friendNamesListFound [l]) {
										friendPrefabFoundList.Add (friendPrefabList [k]);
								}
						}
				}
				
				//position all
				for (int m = 0; m < friendPrefabFoundList.Count; m++) {
						Vector3 friendPreFabPosition = new Vector3 (0f, -25f - (40f * m), 0f);
						friendPrefabFoundList [m].transform.localPosition = friendPreFabPosition;

						prefabInstantiated++;

						if (prefabInstantiated >= 2) {
								prefabInstantiated = 0;

								friendPreFabPosition = new Vector3 (360f, -25f - (40f * (m - 1)), 0f);
								friendPrefabFoundList [m].transform.localPosition = friendPreFabPosition;
						}
				}

				springPanel.GetComponent<UIScrollView>().ResetPosition();
		}

		public void ResetPosition(){
				searchLabelFeedback.enabled = false;
				int prefabInstantiated = 0;

				for (int i = 0; i < friendPrefabList.Count; i++) {
						Vector3 friendPreFabPosition = new Vector3 (0f, -25f - (40f * i), 0f);
						friendPrefabList [i].transform.localPosition = friendPreFabPosition;

						prefabInstantiated++;

						if (prefabInstantiated >= 2) {
								prefabInstantiated = 0;

								friendPreFabPosition = new Vector3 (360f, -25f - (40f * (i - 1)), 0f);
								friendPrefabList [i].transform.localPosition = friendPreFabPosition;
						}
				}

		}

		public void onFriendSelect (string friendName, string friendPhotoURL, string friendID)
		{
				GameManager.tempChallengedFriendName = friendName;
				GameManager.tempChallengedFriendID = friendID;

				char tempChar = friendPhotoURL [8];

				//Debug.Log (friendPhotoURL);
				//Debug.Log (tempChar.ToString ());

				if (tempChar.ToString () == "f") {
						GameManager.tempChallengedFriendPhotoURL = friendPhotoURL;
						GameMenuManager gmManager = GameObject.Find ("GameMenuManager").GetComponent<GameMenuManager> ();
						gmManager.ChallengeCurrentFriend ();
				} else {
						FB.API (friendPhotoURL, Facebook.HttpMethod.GET, result => {
								if (result.Error != null) {
										Util.LogError (result.Error);
										return;
								}

								GameManager.tempChallengedFriendPhotoURL = Util.DeserializePictureURLString (result.Text);
								GameMenuManager gmManager = GameObject.Find ("GameMenuManager").GetComponent<GameMenuManager> ();
								gmManager.ChallengeCurrentFriend ();
						});
				}
		}
				
	
}
