﻿using UnityEngine;
using System.Collections;

public class GamePrefab : MonoBehaviour
{
		
		public GameObject bgSprite;
		public GameObject xSprite;
		public GameObject newGameSprite;
		public GameObject waitingLabel;
		public GameObject waitingSinceLabel;

		public GameObject myNameSprite;
		public GameObject myScoreSprite;
		public GameObject myCards;

		public GameObject friendNameSprite;
		public GameObject friendScoreSprite;
		public GameObject friendCards;

		public UITexture photoTexture;
		public UITexture friendPhotoTexture;

		public bool isNewGame;
		public float gameTime;

		Color32 fontColor = new Color32 (77, 77, 77, 255);

		// Use this for initialization
		void Start ()
		{
				CheckForNewGames ();
				//DownloadImage (GameManager.GetFriendsURL(0) as string);
				DownloadImage("lala");
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void DownloadImage(string url)
		{   
				//StartCoroutine(coDownloadImage(url));

				//StartCoroutine(coDownloadImage("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/v/t1.0-1/p50x50/10155753_10152420965508690_7021049977340795871_n.jpg?oh=e4839c93519f373258f5820d9ad2131c&oe=55DFF95A&__gda__=1436493286_593fb02ca0c11dbbd1651680d9de6709"));
		}

		IEnumerator coDownloadImage(string imageUrl)
		{
				WWW www = new WWW( imageUrl );

				yield return www;
				www.LoadImageIntoTexture (photoTexture.mainTexture as Texture2D);
				www.Dispose();
				www = null;
		}






		void CheckForNewGames ()
		{
				if (!isNewGame) {
						bgSprite.GetComponent<UISprite> ().spriteName = "CurrentGame_BG";
						xSprite.GetComponent<UISprite> ().spriteName = "XSprite_Black";

						myNameSprite.GetComponent<UILabel> ().color = fontColor;
						myScoreSprite.GetComponent<UILabel> ().color = fontColor;
						myCards.GetComponent<UILabel> ().color = fontColor;

						friendNameSprite.GetComponent<UILabel> ().color = fontColor;
						friendScoreSprite.GetComponent<UILabel> ().color = fontColor;
						friendCards.GetComponent<UILabel> ().color = fontColor;

						NGUITools.SetActive (newGameSprite, false);
						NGUITools.SetActive (waitingLabel, true);
						NGUITools.SetActive (waitingSinceLabel, true);
				} else {
						NGUITools.SetActive (newGameSprite, true);
						NGUITools.SetActive (waitingLabel, false);
						NGUITools.SetActive (waitingSinceLabel, false);
				}
		}
}
