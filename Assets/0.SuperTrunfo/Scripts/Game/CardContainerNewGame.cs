﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardContainerNewGame : MonoBehaviour
{

		public int cardNumber;
		public bool isCardChosen;
		public GameObject card_card;
		public GameObject category01;
		public GameObject category02;
		public GameObject category03;
		public GameObject category04;
		public GameObject category05;
		public GameObject category06;
		public GameObject category07;
		public GameObject category08;
		public NewGameWithFriendManager newGameWithFriendManager;
		Color32 fontColor = new Color32 (77, 77, 7, 255);

		// Use this for initialization
		void Start ()
		{
				GameObject tempGo = GameObject.Find ("NewGameWithFriendManager");

				if (tempGo) {
						newGameWithFriendManager = GameObject.Find ("NewGameWithFriendManager").GetComponent<NewGameWithFriendManager> ();
				}

				/*
				gameObject.AddComponent<TweenPosition> ();
				gameObject.GetComponent<TweenPosition> ().method = UITweener.Method.EaseOut;

				gameObject.GetComponent<TweenPosition> ().from = new Vector3 (transform.localPosition.x,
						transform.localPosition.y,
						transform.localPosition.z);

				gameObject.GetComponent<TweenPosition> ().to = new Vector3 (transform.localPosition.x,
						transform.localPosition.y,
						transform.localPosition.z);
						*/

		}

		// Update is called once per frame
		void Update ()
		{

		}

		public void SetCard (string cardName,
		                     string cardGroup,
		                     string cardNumber,
		                     string cardPhoto,
		                     string cardCategory01_value,
		                     string cardCategory02_value,
		                     string cardCategory03_value,
		                     string cardCategory04_value,
		                     string cardCategory05_value,
		                     string cardCategory06_value,
		                     string cardCategory07_value,
		                     string cardCategory08_value)
		{

				card_card.transform.FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = cardGroup + cardNumber;
				card_card.transform.FindChild ("Label_CardName").GetComponent<UILabel> ().text = cardName;
				card_card.transform.FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = cardPhoto;
				category01.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = cardCategory01_value;
				category02.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = cardCategory02_value;
				category03.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = cardCategory03_value;
				category04.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = cardCategory04_value;
				category05.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = cardCategory05_value;
				category06.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = cardCategory06_value;
				category07.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = cardCategory07_value;
				category08.transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = cardCategory08_value;

				category01.GetComponent<CardCategoryNewGame> ().categoryValue = IntParseFast(cardCategory01_value);
				category02.GetComponent<CardCategoryNewGame> ().categoryValue = IntParseFast(cardCategory02_value);
				category03.GetComponent<CardCategoryNewGame> ().categoryValue = IntParseFast(cardCategory03_value);
				category04.GetComponent<CardCategoryNewGame> ().categoryValue = IntParseFast(cardCategory04_value);
				category05.GetComponent<CardCategoryNewGame> ().categoryValue = IntParseFast(cardCategory05_value);
				category06.GetComponent<CardCategoryNewGame> ().categoryValue = IntParseFast(cardCategory06_value);
				category07.GetComponent<CardCategoryNewGame> ().categoryValue = IntParseFast(cardCategory07_value);
				category08.GetComponent<CardCategoryNewGame> ().categoryValue = IntParseFast(cardCategory08_value);

				category01.GetComponent<CardCategoryNewGame> ().categoryNumber = 4;
				category02.GetComponent<CardCategoryNewGame> ().categoryNumber = 5;
				category03.GetComponent<CardCategoryNewGame> ().categoryNumber = 6;
				category04.GetComponent<CardCategoryNewGame> ().categoryNumber = 7;
				category05.GetComponent<CardCategoryNewGame> ().categoryNumber = 8;
				category06.GetComponent<CardCategoryNewGame> ().categoryNumber = 9;
				category07.GetComponent<CardCategoryNewGame> ().categoryNumber = 10;
				category08.GetComponent<CardCategoryNewGame> ().categoryNumber = 11;
		}

		public void SelectCategory (string categoryLabel, int categoryNumber, int categoryValue, int cardNumber, GameObject categoryGameObject)
		{
				Debug.Log ("Card category: " + categoryLabel + " | " + 
						"Category Number: " + categoryNumber + " | " +
						"Category Value: " + categoryValue + " | " + 
						"Card number: " + cardNumber);

				if (newGameWithFriendManager) {
						newGameWithFriendManager.CheckCardCategory (categoryNumber, categoryValue, categoryGameObject);
				}
				
				isCardChosen = true;

				ResetAllCategoriesLayout ();

				//categoryGameObject.GetComponent<TweenColor> ().enabled = false;
				//categoryGameObject.GetComponent<UIButton> ().ResetDefaultColor ();

				categoryGameObject.GetComponent<BoxCollider> ().enabled = false;
				categoryGameObject.GetComponent<UISprite> ().color = Color.white;
				categoryGameObject.GetComponent<UISprite> ().spriteName = "CardSpecBG_Selected";
				categoryGameObject.GetComponent<UISprite> ().transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
				categoryGameObject.GetComponent<UISprite> ().transform.FindChild ("Label_Points").GetComponent<UILabel> ().color = fontColor;

		}

		public void ResetAllCategoriesLayout ()
		{
				GameObject card = transform.FindChild ("Card").gameObject;

				List<GameObject> myGoList = new List<GameObject> ();

				myGoList.Add (card.transform.FindChild ("Sprite_Category_Potencia").gameObject);
				myGoList.Add (card.transform.FindChild ("Sprite_Category_Torque").gameObject);
				myGoList.Add (card.transform.FindChild ("Sprite_Category_Consumo").gameObject);
				myGoList.Add (card.transform.FindChild ("Sprite_Category_Ruido").gameObject);
				myGoList.Add (card.transform.FindChild ("Sprite_Category_Aceleracao").gameObject);
				myGoList.Add (card.transform.FindChild ("Sprite_Category_Frenagem").gameObject);
				myGoList.Add (card.transform.FindChild ("Sprite_Category_Retomada").gameObject);
				myGoList.Add (card.transform.FindChild ("Sprite_Category_PortaMalas").gameObject);

				for (int i = 0; i < myGoList.Count; i++) {
						myGoList [i].GetComponent<UISprite> ().spriteName = "CardSpecBG";
						myGoList [i].GetComponent<UISprite> ().transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
						myGoList [i].GetComponent<UISprite> ().transform.FindChild ("Label_Points").GetComponent<UILabel> ().color = Color.white;
						myGoList [i].GetComponent<UIButton> ().ResetDefaultColor ();
						myGoList [i].GetComponent<BoxCollider> ().enabled = true;

						myGoList [i].GetComponent<CardCategoryNewGame>().ResetSprites();

						if (myGoList [i].GetComponent<TweenColor> ()) {
								myGoList [i].GetComponent<TweenColor> ().to = Color.white;
						}
				}
		}

		public static int IntParseFast (string value)
		{
				int result = 0;
				for (int i = 0; i < value.Length; i++) {
						char letter = value [i];
						result = 10 * result + (letter - 48);
				}
				return result;
		}



}
