﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneManager : MonoBehaviour
{
		// Use this for initialization
		void Start ()
		{
				//DontDestroyOnLoad (transform.gameObject);
			AudioManager.Instance.CheckSceneAndUpdate();
		}

		// Update is called once per frame
		void Update ()
		{

		}

		public static void GoToMainScene ()
		{
				Debug.Log ("[Super Trunfo]: Loading MainScene");
				Application.LoadLevel ("01.MainScene");
				//Application.LoadLevel ("10.SharePhotoScene");
				Debug.Log ("[Super Trunfo]: MainScene Loaded");
		}

		public void GoToGameWithFriends ()
		{
				LoadScene ();
		}

		public void LoadSinglePlayer ()
		{
				Application.LoadLevel ("08.SinglePlayerScene");
		}

		public void LoadScene ()
		{
				Debug.Log ("[Super Trunfo]: Loading GameMenuScene");
				Application.LoadLevel ("02.GameMenuScene");
				Debug.Log ("[Super Trunfo]: MainScene GameMenuScene");
		}

	public void postFeed(){
		Application.LoadLevel ("10.SharePhotoScene");
		/*
		FB.Feed(
			linkCaption: "Eu ganhei um Duelo Quatro Rodas! Desafio você a fazer o mesmo!",
			picture: "http://www.friendsmash.com/images/logo_large.jpg",
			linkName: "Duelo Quatro Rodas",
			link: "http://apps.facebook.com/" + FB.AppId + "/?challenge_brag=" + (FB.IsLoggedIn ? FB.UserId : "guest")
			);
		*/
	}
}
