﻿using UnityEngine;
using System.Collections;

public class RulesScene : MonoBehaviour {

	public GameObject rules01;
	public GameObject rules02;
	public GameObject rules03;

	public UISprite nextArrow;
	public UISprite prevArrow;

	public GameObject helpPanel;

	int currentRule = 1;

	// Use this for initialization
	void Start () {
		BackToMenu();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void NextRule(){
		currentRule++;
		if(currentRule == 1){
			NGUITools.SetActive(rules01, true);
			NGUITools.SetActive(rules02, false);
			NGUITools.SetActive(rules03, false);
			nextArrow.spriteName = "Arrow";
			prevArrow.spriteName = "GreyArrow";
			prevArrow.enabled = false;
		}
		if(currentRule == 2){
			NGUITools.SetActive(rules01, false);
			NGUITools.SetActive(rules02, true);
			NGUITools.SetActive(rules03, false);
			nextArrow.spriteName = "Arrow";
			prevArrow.spriteName = "Arrow";
			nextArrow.enabled = true;
			prevArrow.enabled = true;
		}
		if(currentRule == 3){
			NGUITools.SetActive(rules01, false);
			NGUITools.SetActive(rules02, false);
			NGUITools.SetActive(rules03, true);
			nextArrow.spriteName = "GreyArrow";
			prevArrow.spriteName = "Arrow";
			nextArrow.enabled = false;
		}

		if(currentRule >= 3){
			currentRule = 3;
		}
	}

	public void PrevRule(){
		currentRule--;
		if(currentRule == 1){
			NGUITools.SetActive(rules01, true);
			NGUITools.SetActive(rules02, false);
			NGUITools.SetActive(rules03, false);
			nextArrow.spriteName = "Arrow";
			prevArrow.spriteName = "GreyArrow";
			prevArrow.enabled = false;
		}
		if(currentRule == 2){
			NGUITools.SetActive(rules01, false);
			NGUITools.SetActive(rules02, true);
			NGUITools.SetActive(rules03, false);
			nextArrow.spriteName = "Arrow";
			prevArrow.spriteName = "Arrow";
			nextArrow.enabled = true;
			prevArrow.enabled = true;
		}
		if(currentRule == 3){
			NGUITools.SetActive(rules01, false);
			NGUITools.SetActive(rules02, false);
			NGUITools.SetActive(rules03, true);
			nextArrow.spriteName = "GreyArrow";
			prevArrow.spriteName = "Arrow";
			nextArrow.enabled = false;
		}

		if(currentRule <= 1){
			currentRule = 1;
		}
	}

	public void ShowHelp(){
		//NGUITools.SetActive(helpPanel, true);

		Vector3 newPosition = new Vector3(0,0,0);
		helpPanel.transform.localPosition = newPosition;

	}

	public void BackToMenu(){
		//NGUITools.SetActive(helpPanel, false);

		Vector3 newPosition = new Vector3(-3000,0,0);
		helpPanel.transform.localPosition = newPosition;
	}
}
