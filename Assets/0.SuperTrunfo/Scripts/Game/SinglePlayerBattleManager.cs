﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SinglePlayerBattleManager : MonoBehaviour
{
	
		public GameObject feedBackContainer;
		public GameObject categoryPlayer01;
		public GameObject categoryPlayer02;
		public GameObject battlePanel;
		//public UILabel roundLabel;
		public UILabel titleLabel;
		public UILabel cardsLabel;
		public UILabel feedbackLabel;
		public UISprite feedbackSprite;
		string labelWinner = "BOA JOGADA! ESSA CARTA É SUA!";
		string labelLoser = "OPA! PERDEU UMA CARTA!";
		List<float> categoriesPositions = new List<float> ();
		int currentRound = 1;
		Color32 fontColor = new Color32 (139, 135, 136, 255);
		public CardContainerNewGame playerCardContainer;
		public CardContainerNewGame computerCardContainer;
		int playerCard;
		int enemyCard;
		int playerTotalCards;
		int enemyTotalCards;
		public UILabel playerTotalCardsLabel;
		public UILabel enemyTotalCardsLabel;
		public UILabel playerCategoryFeedbackLabel;
		public UILabel enemyCategoryFeedbackLabel;
		public UILabel playerCategoryValueFeedbackLabel;
		public UILabel enemyCategoryValueFeedbackLabel;
		public UILabel playerName;
		public GameObject computerFrontCard;
		public GameObject containerBlocker;
		public UITexture userPhoto;
		public GameObject anim01;
		public UIPanel feedbackPanel;
		private int categoryNumberInGame;
		public UIWidget cardPlayer01;
		public UIWidget cardPlayer02;
		
		// Use this for initialization
		void Awake ()
		{		
				CardDeck.singlePlayerDeck.Clear ();
				CardDeck.ArrangeHalfDeck ();
				CardDeck.ShuffleHalfDeck ();
		}

		void Start ()
		{
				
				playerName.text = GameManager.jogadorUserName;
				if (!GameManager.isLocal) {
						AudioManager.Instance.CheckSceneAndUpdate ();
						GameManager.DownloadImage (GameManager.jogadorUserPhotoURL, userPhoto);
				}
	
				categoriesPositions.Add (25);
				categoriesPositions.Add (-10);
				categoriesPositions.Add (-45);
				categoriesPositions.Add (-80);
				categoriesPositions.Add (-115);
				categoriesPositions.Add (-150);
				categoriesPositions.Add (-185);
				categoriesPositions.Add (-220);

				GameObject painel = GameObject.Find ("Panel_Battle");
				NGUITools.SetActive (painel, false);

				ShuffleCardsAndDivide ();
		}

		void ShuffleCardsAndDivide ()
		{
				CardDeck.userDeck.Clear ();
				CardDeck.friendDeck.Clear ();
				
				for (int i = 0; i < CardDeck.singlePlayerDeck.Count / 2; i++) {
						CardDeck.userDeck.Add (CardDeck.singlePlayerDeck [i]);
				}

				for (int j = CardDeck.singlePlayerDeck.Count / 2; j < CardDeck.singlePlayerDeck.Count; j++) {
						CardDeck.friendDeck.Add (CardDeck.singlePlayerDeck [j]);
				}

				playerTotalCards = CardDeck.userDeck.Count;
				enemyTotalCards = CardDeck.friendDeck.Count;

				playerCard = 0;	//Random.Range (0, CardDeck.userDeck.Count);
				enemyCard = 0;	//Random.Range (0, CardDeck.friendDeck.Count);

				playerTotalCardsLabel.text = playerTotalCards.ToString ();
				enemyTotalCardsLabel.text = enemyTotalCards.ToString ();

				StartBattle ();
		}

		void StartBattle ()
		{
				playerCardContainer.SetCard (CardDeck.userDeck [playerCard] [0], 
						CardDeck.userDeck [playerCard] [1],
						CardDeck.userDeck [playerCard] [2],
						CardDeck.userDeck [playerCard] [12],
						CardDeck.userDeck [playerCard] [4],
						CardDeck.userDeck [playerCard] [5],
						CardDeck.userDeck [playerCard] [6],
						CardDeck.userDeck [playerCard] [7],
						CardDeck.userDeck [playerCard] [8],
						CardDeck.userDeck [playerCard] [9],
						CardDeck.userDeck [playerCard] [10],
						CardDeck.userDeck [playerCard] [11]);


				computerCardContainer.SetCard (CardDeck.friendDeck [enemyCard] [0], 
						CardDeck.friendDeck [enemyCard] [1],
						CardDeck.friendDeck [enemyCard] [2],
						CardDeck.friendDeck [enemyCard] [12],
						CardDeck.friendDeck [enemyCard] [4],
						CardDeck.friendDeck [enemyCard] [5],
						CardDeck.friendDeck [enemyCard] [6],
						CardDeck.friendDeck [enemyCard] [7],
						CardDeck.friendDeck [enemyCard] [8],
						CardDeck.friendDeck [enemyCard] [9],
						CardDeck.friendDeck [enemyCard] [10],
						CardDeck.friendDeck [enemyCard] [11]);
		}

		public void CategorySelected (int categoryNumber, int categoryValue)
		{
				containerBlocker.GetComponent<BoxCollider> ().enabled = true;

				//print ("Jogador tem: " + CardDeck.deck [playerCard] [categoryNumber]);
				//print ("Amigo tem: " + CardDeck.deck [enemyCard] [categoryNumber]);

				computerFrontCard.GetComponent<TweenRotation> ().ResetToBeginning ();
				computerFrontCard.GetComponent<TweenRotation> ().PlayForward ();

				//NewFeedbackPosition (categoryNumber, CardDeck.userDeck [0] [categoryNumber], CardDeck.friendDeck [0] [categoryNumber]);
				NewFeedbackPosition (categoryNumber,
		                     		CardDeck.userDeck [0] [12],
		                     		CardDeck.userDeck [0] [1] + CardDeck.userDeck [0] [2],
		                     		CardDeck.userDeck [0] [0],
				                    CardDeck.friendDeck [0] [12],
		                     		CardDeck.friendDeck [0] [1] + CardDeck.friendDeck [0] [2],
				                    CardDeck.friendDeck [0] [0],
				                    CardDeck.userDeck [0] [categoryNumber], 
				                    CardDeck.friendDeck [0] [categoryNumber]);


				if (CardDeck.userDeck [0] [categoryNumber] == "n/d") {
						CheckWhosWinner (0, float.Parse (CardDeck.friendDeck [0] [categoryNumber], System.Globalization.CultureInfo.GetCultureInfo ("pt-BR").NumberFormat));
				} else if (CardDeck.friendDeck [0] [categoryNumber] == "n/d") {
						CheckWhosWinner (float.Parse (CardDeck.userDeck [0] [categoryNumber], System.Globalization.CultureInfo.GetCultureInfo ("pt-BR").NumberFormat), 0);
				} else if (CardDeck.userDeck [0] [categoryNumber] == "n/d" && CardDeck.friendDeck [0] [categoryNumber] == "n/d") {
						CheckWhosWinner (0, 0);
				} else {
						CheckWhosWinner (float.Parse (CardDeck.userDeck [0] [categoryNumber], System.Globalization.CultureInfo.GetCultureInfo ("pt-BR").NumberFormat), float.Parse (CardDeck.friendDeck [0] [categoryNumber], System.Globalization.CultureInfo.GetCultureInfo ("pt-BR").NumberFormat));
				}
		}

		public void ShowFeedbackPanel ()
		{
				StartCoroutine (ShowFeed ());
		}

		IEnumerator ShowFeed ()
		{
				yield return new WaitForSeconds (0.75f);
				NGUITools.SetActive (battlePanel, true);
				anim01.GetComponent<iTweenEvent> ().Play ();
		}

		public void CheckWhosWinner (float playerValue, float enemyValue)
		{
				if (categoryNumberInGame == 4 ||
						categoryNumberInGame == 5 ||
						categoryNumberInGame == 6 ||
						categoryNumberInGame == 11) {
						if (playerValue > enemyValue) {
								//Player01 Wins
								categoryPlayer01.GetComponent<UISprite> ().spriteName = "CategoryWinner";
								categoryPlayer02.GetComponent<UISprite> ().spriteName = "CategoryLoser";
				
								categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
								categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
				
								feedbackLabel.text = labelWinner;
								feedbackSprite.spriteName = "Shining";
								playerTotalCards++;
								enemyTotalCards--;
				
				
								CardDeck.userDeck.Add (CardDeck.userDeck [playerCard]);
								CardDeck.userDeck.Add (CardDeck.friendDeck [enemyCard]);
								CardDeck.userDeck.RemoveAt (0);
								CardDeck.friendDeck.RemoveAt (0);

						} else {
								//Player02 Wins
								categoryPlayer01.GetComponent<UISprite> ().spriteName = "CategoryLoser";
								categoryPlayer02.GetComponent<UISprite> ().spriteName = "CategoryWinner";
				
								categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
								categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
				
								feedbackLabel.text = labelLoser;
								feedbackSprite.spriteName = "Shining_Loser";
								playerTotalCards--;
								enemyTotalCards++;
				
								CardDeck.friendDeck.Add (CardDeck.friendDeck [enemyCard]);
								CardDeck.friendDeck.Add (CardDeck.userDeck [playerCard]);

								CardDeck.userDeck.RemoveAt (0);
								CardDeck.friendDeck.RemoveAt (0);
						}
				}

				if (categoryNumberInGame == 7 ||
						categoryNumberInGame == 8 ||
						categoryNumberInGame == 9 ||
						categoryNumberInGame == 10) {
						if (playerValue < enemyValue) {
								//Player01 Wins
								categoryPlayer01.GetComponent<UISprite> ().spriteName = "CategoryWinner";
								categoryPlayer02.GetComponent<UISprite> ().spriteName = "CategoryLoser";
				
								categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
								categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
				
								feedbackLabel.text = labelWinner;
								feedbackSprite.spriteName = "Shining";
								playerTotalCards++;
								enemyTotalCards--;
				
				
								CardDeck.userDeck.Add (CardDeck.userDeck [playerCard]);
								CardDeck.userDeck.Add (CardDeck.friendDeck [enemyCard]);
								CardDeck.userDeck.RemoveAt (0);
								CardDeck.friendDeck.RemoveAt (0);
				
						} else {
								//Player02 Wins
								categoryPlayer01.GetComponent<UISprite> ().spriteName = "CategoryLoser";
								categoryPlayer02.GetComponent<UISprite> ().spriteName = "CategoryWinner";
				
								categoryPlayer01.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = fontColor;
								categoryPlayer02.transform.FindChild ("Label_Category").GetComponent<UILabel> ().color = Color.white;
				
								feedbackLabel.text = labelLoser;
								feedbackSprite.spriteName = "Shining_Loser";
								playerTotalCards--;
								enemyTotalCards++;
				
								CardDeck.friendDeck.Add (CardDeck.friendDeck [enemyCard]);
								CardDeck.friendDeck.Add (CardDeck.userDeck [playerCard]);
				
								CardDeck.userDeck.RemoveAt (0);
								CardDeck.friendDeck.RemoveAt (0);
						}
				}
				Debug.Log ("User: " + CardDeck.userDeck.Count + " vs. " + CardDeck.friendDeck.Count + " :Friend");
		}

		//void NewFeedbackPosition (int categoryNumber, string playerCategoryValue, string enemyCategoryValue)
		void NewFeedbackPosition (int categoryNumber,
	                          string p1CarPhoto,
	                          string p1CarNumber,
	                          string p1CarName,
	                          string p2CarPhoto,
	                          string p2CarNumber,
	                          string p2CarName,
	                          string playerCategoryValue,
	                          string enemyCategoryValue)
		{
				categoryNumberInGame = categoryNumber;

				
				Vector3 newContainerPosition = new Vector3 (0, 0, 0);
				
				if (categoryNumber == 4) {
						newContainerPosition = new Vector3 (0, categoriesPositions [0], 0);
						playerCategoryFeedbackLabel.text = "Potência";
				}
				if (categoryNumber == 5) {
						newContainerPosition = new Vector3 (0, categoriesPositions [1], 0);
						playerCategoryFeedbackLabel.text = "Torque";
				}
				if (categoryNumber == 6) {
						newContainerPosition = new Vector3 (0, categoriesPositions [2], 0);
						playerCategoryFeedbackLabel.text = "Cons. urb";
				}
				if (categoryNumber == 7) {
						newContainerPosition = new Vector3 (0, categoriesPositions [3], 0);
						playerCategoryFeedbackLabel.text = "Ruído p.m";
				}
				if (categoryNumber == 8) {
						newContainerPosition = new Vector3 (0, categoriesPositions [4], 0);
						playerCategoryFeedbackLabel.text = "Aceleração";
				}
				if (categoryNumber == 9) {
						newContainerPosition = new Vector3 (0, categoriesPositions [5], 0);
						playerCategoryFeedbackLabel.text = "Fren. 120-0";
				}
				if (categoryNumber == 10) {
						newContainerPosition = new Vector3 (0, categoriesPositions [6], 0);
						playerCategoryFeedbackLabel.text = "Retom 40-80";
				}
				if (categoryNumber == 11) {
						newContainerPosition = new Vector3 (0, categoriesPositions [7], 0);
						playerCategoryFeedbackLabel.text = "Porta-malas";
				}

						
				enemyCategoryFeedbackLabel.text = playerCategoryFeedbackLabel.text;

				playerCategoryValueFeedbackLabel.text = playerCategoryValue;
				enemyCategoryValueFeedbackLabel.text = enemyCategoryValue;

				//feedBackContainer.transform.localPosition = newContainerPosition;

				cardPlayer01.transform.FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = p1CarPhoto;
				cardPlayer01.transform.FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = p1CarNumber;
				cardPlayer01.transform.FindChild ("Label_CardName").GetComponent<UILabel> ().text = p1CarName;

				cardPlayer02.transform.FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = p2CarPhoto;
				cardPlayer02.transform.FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = p2CarNumber;
				cardPlayer02.transform.FindChild ("Label_CardName").GetComponent<UILabel> ().text = p2CarName;
		}

		public void NextCard ()
		{

				if (playerTotalCards >= CardDeck.singlePlayerDeck.Count) {
						CardDeck.singlePlayerDeck.Clear ();
						Application.LoadLevel ("09.SinglePlayerSceneGameWin");
						return;
				}

				if (enemyTotalCards >= CardDeck.singlePlayerDeck.Count) {
						CardDeck.singlePlayerDeck.Clear ();
						Application.LoadLevel ("09.SinglePlayerSceneGameOver");
						return;
				}

				currentRound++;
				if (currentRound < 10) {
						titleLabel.text = "RODADA 0" + currentRound;	
				} else {
						titleLabel.text = "RODADA " + currentRound;
				}

				containerBlocker.GetComponent<BoxCollider> ().enabled = false;
				NGUITools.SetActive (battlePanel, false);
				playerCardContainer.ResetAllCategoriesLayout ();

				//playerCard = Random.Range (0, CardDeck.userDeck.Count);
				//enemyCard = Random.Range (0, CardDeck.friendDeck.Count);

				playerTotalCardsLabel.text = playerTotalCards.ToString ();
				enemyTotalCardsLabel.text = enemyTotalCards.ToString ();

				Quaternion newRotation = new Quaternion (0, 0, 0, 0);
				computerFrontCard.transform.localRotation = newRotation;

				playerCardContainer.GetComponent<TweenPosition> ().ResetToBeginning ();
				playerCardContainer.GetComponent<TweenPosition> ().delay = 0;
				playerCardContainer.GetComponent<TweenPosition> ().PlayForward ();

				computerFrontCard.GetComponent<TweenPosition> ().ResetToBeginning ();
				computerFrontCard.GetComponent<TweenPosition> ().delay = 0;
				computerFrontCard.GetComponent<TweenPosition> ().PlayForward ();

				StartBattle ();
		}

		void NextScene ()
		{
				Application.LoadLevel ("07.CardBattleFinishedScene");
		}

		public void OpenExitFeedback ()
		{
				feedbackPanel.transform.localPosition = new Vector3 (0, 0, 0);
		}

		public void CloseExitFeedback ()
		{
				feedbackPanel.transform.localPosition = new Vector3 (-3000, 0, 0);
		}

		public void BackMenu ()
		{
				Application.LoadLevel ("01.MainScene");
		}
	
		/*
		public static void ShuffleList<T>(this IList<T> ts) {
			int count = ts.Count;
			int last = count - 1;
			for (int i = 0; i < last; ++i) {
				var r = Random.Range(i, count);
				var tmp = ts[i];
				ts[i] = ts[r];
				ts[r] = tmp;
			}
		}
		*/
	
		public static void ShuffleArray<T> (T[] arr)
		{
				for (int i = arr.Length - 1; i > 0; i--) {
						int r = Random.Range (0, i + 1);
						T tmp = arr [i];
						arr [i] = arr [r];
						arr [r] = tmp;
				}
		}

		public static float IntParseFast (string value)
		{
				float result = 0;
				for (int i = 0; i < value.Length; i++) {
						char letter = value [i];
						result = 10 * result + (letter - 48);
				}
				return result;
		}
	
}