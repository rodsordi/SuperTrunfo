﻿using UnityEngine;
using System.Collections;

public class SinglePlayerGameWin : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
		AudioManager.Instance.CheckSceneAndUpdate();
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void Share ()
		{
				FB.Feed (
						linkCaption: "Eu venci o Duelo Quatro Rodas! Sera que você consegue também?",
						picture: GameManager.mainURL + "GanheiDuelo04Rodas.png",
						linkName: "Duelo Quatro Rodas",
						link: "http://apps.facebook.com/" + FB.AppId + "/?challenge_brag=" + (FB.IsLoggedIn ? FB.UserId : "guest")
				);
		}

		public void BackToMenu ()
		{
				Application.LoadLevel ("01.MainScene");
		}
}
