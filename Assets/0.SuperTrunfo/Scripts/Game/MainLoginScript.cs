﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class MainLoginScript : MonoBehaviour
{

	//string meQueryString = "/v2.0/me?fields=id,first_name,friends.limit(100).fields(first_name,id,picture.width(128).height(128)),invitable_friends.limit(100).fields(first_name,id,picture.width(128).height(128))";
	string meQueryString = "/v2.2/me?fields=id,first_name";
	string friendsQuery = "/v2.2/me?fields=friends";
	string photosQuery = "/v2.2/me/photos?fields=source";

	//private static List<object> friends = null;
	private static Dictionary<string, string> profile = null;
	//private static List<object> photosImages = null;
	//private static List<object> scores = null;
	//private static Dictionary<string, Texture> friendImages = new Dictionary<string, Texture> ();

	public GameObject userNameLabel;
	public GameObject userFriendsTotal;


	// Use this for initialization
	void Start ()
	{
		Debug.Log ("@@@@ Initing...");
		InitFacebookLogin ();
	}

	void InitFacebookLogin ()
	{
		FB.Init (SetInit);
	}

	public void LoginWithFacebook ()
	{
		FB.Login ("public_profile,user_friends,user_photos,email,publish_actions", LoginCallback);
	}

	void SetInit ()
	{
		if (FB.IsLoggedIn) {
			OnLoggedIn ();
		}
	}
		
	void OnLoggedIn ()
	{
		Debug.Log ("@@@@ OnLoggedIn...");
		// Request player info and profile picture
		FB.API (meQueryString, Facebook.HttpMethod.GET, APICallback);
		//LoadPictureAPI(Util.GetPictureURL("me", 128, 128),MyPictureCallback);
	}

	void GetPhotos ()
	{
		Debug.Log ("@@@@ Getting photos...");
		FB.API (photosQuery, Facebook.HttpMethod.GET, GetPhotosCallback);
	}

	void GetFriends ()
	{
		Debug.Log ("@@@@ Getting friends...");
		FB.API (friendsQuery, Facebook.HttpMethod.GET, GetFriendsCallback);
	}



	//#########
	//CALLBACKS
	//#########
	void LoginCallback (FBResult result)
	{
		if (FB.IsLoggedIn) {
			OnLoggedIn ();
		}
	}

	void APICallback (FBResult result)
	{
		Debug.Log ("@@@@ APICallback...");
		GetPhotos ();

		profile = Util.DeserializeJSONProfile (result.Text);
		userNameLabel.GetComponent<UILabel>().text = profile ["first_name"];

		Debug.Log ("@@@@ User name: " + profile ["first_name"]);
		//GameStateManager.Username = profile["first_name"];
		//friends = Util.DeserializeJSONFriends(result.Text);
	}

	void MyPictureCallback (Texture texture)
	{
		//GameStateManager.UserTexture = texture;
	}

	void GetPhotosCallback (FBResult result)
	{
		GetFriends ();

		//photosImages = new List<object> ();
		List<object> photosImagesList = Util.DeserializeJSONPhoto (result.Text);
		foreach (object photoImage in photosImagesList) {
			var imagesData = (Dictionary<string,object>)photoImage;
			object photoURL;

			//This is a safer, but slow, method of accessing values in a dictionary.
			if (imagesData.TryGetValue ("source", out photoURL)) {
				//Debug.Log(photoURL);
				//print (photoURL);
			} else {
				//failure!
			}
		}
	}

	void GetFriendsCallback(FBResult result){

		Debug.Log (result.Text);

		//friends = Util.DeserializeJSONFriends(result.Text);
		/*
		var responseObject = Json.Deserialize(result.Text) as Dictionary<string, object>;
		List<object> friends = responseObject["id"] as List<object>;

		Debug.Log (friends[0]);

		Dictionary<string,object> scoreData = friends[0] as Dictionary<string,object>;
		object score = scoreData["total_count"];



		Debug.Log ("@@@@ Total friends: " + score);

*/
		/*

		{"name": "Jason Stringe", "user_id": "75782347", "friends": {"data": [{"first_name": "Sally", "user_id": "98198298"}]}}

		{"friends":{"data":[],"summary":{"total_count":323}},"id":"823142771089214"}



		var dict = Json.Deserialize(result.Text) as Dictionary<string,object>;
		string userName = dict["name"];

		object friendsH;
		var friends = new List<object>();
		string friendName;
		if(dict.TryGetValue ("friends", out friendsH)) {
			friends = (List<object>)(((Dictionary<string, object>)friendsH) ["data"]);
			if(friends.Count > 0) {
				var friendDict = ((Dictionary<string,object>)(friends[0]));
				var friend = new Dictionary<string, string>();
				friend["id"] = (string)friendDict["id"];
				friend["first_name"] = (string)friendDict["first_name"];
			}
		}

		*/

		DoTest (result.Text);
	}

	public void DoTest(string jsonString)
	{
		//string jsonString = "{ \"int\" : 1, \"array\" : { \"int\" : 2, \"numbers\": { \"number1\" : 1 }}}";
		int count = 0;
		ParseJSON( 0, Json.Deserialize( jsonString ) as Dictionary<string, object>, ref count );
		Debug.Log( "Number of keys: " + count );
	}


	private void ParseJSON( int level, Dictionary<string, object> dictToParse, ref int count )
	{
		if( dictToParse != null )
		{
			foreach( KeyValuePair< string, object > pair in dictToParse )
			{
				if( pair.Value is Dictionary< string, object > ) {
					// Nested node
					ParseJSON( level + 1, pair.Value as Dictionary< string, object >, ref count );
				} else {
					// Item node
					count++;
					Debug.Log( "Level: " + level + ", Key: " + pair.Key + ", Value: " + pair.Value.ToString() );
					if (pair.Key == "total_count") {
						userFriendsTotal.GetComponent<UILabel> ().text = pair.Value.ToString ();
					}
				}
			}
		}
	}



	//Self Picture

	delegate void LoadPictureCallback (Texture texture);

	IEnumerator LoadPictureEnumerator (string url, LoadPictureCallback callback)
	{
		WWW www = new WWW (url);
		yield return www;
		callback (www.texture);
	}

	void LoadPictureAPI (string url, LoadPictureCallback callback)
	{
		FB.API (url, Facebook.HttpMethod.GET, result => {
			if (result.Error != null) {
				Util.LogError (result.Error);
				return;
			}

			var imageUrl = Util.DeserializePictureURLString (result.Text);

			Debug.Log ("[PicturePhoto]: " + result.Text);

			StartCoroutine (LoadPictureEnumerator (imageUrl, callback));
		});
	}

	void LoadPictureURL (string url, LoadPictureCallback callback)
	{
		StartCoroutine (LoadPictureEnumerator (url, callback));

	}

}
