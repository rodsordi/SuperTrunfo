﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class NewGameWithFriendManager : MonoBehaviour
{
		private JogadorDAO jDAO = new JogadorDAO ();
		//private CartaDAO cDAO = new CartaDAO ();
		private PartidaDAO pDAO = new PartidaDAO ();

		public CardContainerNewGame cardPrefab;
		public List<CardContainerNewGame> cardContainerList = new List<CardContainerNewGame> ();

		public int[] gameCardsCategoryNumberSelected;
		public int[] gameCardsCategoryValueSelected;
		//private int runningRoutines = 0;

		bool nextButtonEnabled;

		void Awake ()
		{
				CardDeck.ArrangeDeck ();
				CardDeck.ShuffleDeck ();
				GameManager.CreateLists ();
		}

		// Use this for initialization
		void Start ()
		{
				gameCardsCategoryNumberSelected = new int[4];
				gameCardsCategoryValueSelected = new int[4];

				Debug.Log ("[Super Trunfo]: Loading ChallengeFriendScene");
				GameObject.Find ("Button_Proximo").GetComponent<UIButton> ().isEnabled = false;


				Partida partida = GameController.Partida;

				if (partida != null) {
						if (partida.StatusPartida == Partida.STATUS_JOGANDO) {
								CardDeck.ArrangeDeck ();
								CardDeck.ShuffleDeck ();
								UpdateGameWithFriend ();
						}else{
							CreateCards ();
						}	
				} else {
						CreateCards ();
				}

		}

		// Update is called once per frame
		void Update ()
		{
				if (!nextButtonEnabled) {
						int cardsChosen = 0;
						for (int i = 0; i < cardContainerList.Count; i++) {
								if (cardContainerList [i].isCardChosen) {
										cardsChosen++;
								}
						}

						if (cardsChosen >= 4) {
								nextButtonEnabled = true;
								GameObject.Find ("Button_Proximo").GetComponent<UIButton> ().isEnabled = true;
						}
				}
		}

		void CreateCards ()
		{
				//Deck sorteado
				GameManager.tempUserCards.Clear ();
				GameManager.tempFriendCards.Clear ();
				GameManager.tempUserHandCards.Clear ();
				GameManager.tempFriendHandCards.Clear ();

				for (int i = 0; i < 16; i++) {
						GameManager.tempUserCards.Add (CardDeck.deck [i] [13]);
				}

				//CardDeck.deck [i] = { "Jaguar F-Type V8 S", "A", "01", "01.jpg", "495", "63,7", "7,1",  "47,7", "4,2",  "57,3", "1,9", "200", "1" };
				//CardDeck.deck [i][12] = "1"


				for (int j = 16; j < 32; j++) {
						GameManager.tempFriendCards.Add (CardDeck.deck [j] [13]);
				}

				
						

				//Mao
				GameManager.tempUserHandCards.Add (GameManager.tempUserCards [0]);
				GameManager.tempUserHandCards.Add (GameManager.tempUserCards [1]);
				GameManager.tempUserHandCards.Add (GameManager.tempUserCards [2]);
				GameManager.tempUserHandCards.Add (GameManager.tempUserCards [3]);


				GameManager.tempFriendHandCards.Add (GameManager.tempFriendCards [0]);
				GameManager.tempFriendHandCards.Add (GameManager.tempFriendCards [1]);
				GameManager.tempFriendHandCards.Add (GameManager.tempFriendCards [2]);
				GameManager.tempFriendHandCards.Add (GameManager.tempFriendCards [3]);

				


				for (int i = 0; i < 4; i++) {
						Vector3 cardPrefabPosition = new Vector3 (-1200, 0, 0);
						Vector3 cardPrefabScale = new Vector3 (1, 1, 1);

						CardContainerNewGame myCardPrefab = Instantiate (cardPrefab, cardPrefabPosition, Quaternion.identity) as CardContainerNewGame;
						myCardPrefab.name = "CardContainer0" + (i + 1);

						myCardPrefab.transform.parent = GameObject.Find ("PanelGame").transform;
						myCardPrefab.transform.localScale = cardPrefabScale;
						//myCardPrefab.transform.localPosition = new Vector3 (-390f + (260 * i), -30f, 0f);
						myCardPrefab.cardNumber = i + 1;
						cardContainerList.Add (myCardPrefab);

						myCardPrefab.GetComponent<TweenPosition> ().delay = i * 0.3f + 1;
						myCardPrefab.GetComponent<TweenPosition> ().duration = 1;
						myCardPrefab.GetComponent<TweenPosition> ().from = new Vector3 (-1200, 0, 0);
						myCardPrefab.GetComponent<TweenPosition> ().to = new Vector3 (-390f + (260 * i), -30f, 0f);
						myCardPrefab.GetComponent<TweenPosition> ().PlayForward ();

						int cardOnDeck = IntParseFast (GameManager.tempUserHandCards [i].ToString ());
						string[] userCard = null;

						for (int j = 0; j < CardDeck.deck.Count; j++) {
								if (IntParseFast (CardDeck.deck [j] [13]) == cardOnDeck) {
										userCard = CardDeck.deck [j];
								}
						}
								
						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = userCard [12];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = userCard [1] + userCard [2];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardName").GetComponent<UILabel> ().text = userCard [0];

						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Potencia").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [4];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Torque").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [5];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Consumo").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [6];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Ruido").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [7];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Aceleracao").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [8];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Frenagem").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [9];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Retomada").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [10];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_PortaMalas").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [11];


				}
		}


		void UpdateGameWithFriend ()
		{
				Partida partida = GameController.Partida;

				if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
						partida.Turno = Partida.TURNO_ADVERSARIO;

				} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
						partida.Turno = Partida.TURNO_JOGADOR;
				}

				for (int i = 0; i < 4; i++) {
						Vector3 cardPrefabPosition = new Vector3 (-1200, 0, 0);
						Vector3 cardPrefabScale = new Vector3 (1, 1, 1);

						CardContainerNewGame myCardPrefab = Instantiate (cardPrefab, cardPrefabPosition, Quaternion.identity) as CardContainerNewGame;
						myCardPrefab.name = "CardContainer0" + (i + 1);

						myCardPrefab.transform.parent = GameObject.Find ("PanelGame").transform;
						myCardPrefab.transform.localScale = cardPrefabScale;
						//myCardPrefab.transform.localPosition = new Vector3 (-390f + (260 * i), -30f, 0f);
						myCardPrefab.cardNumber = i + 1;
						cardContainerList.Add (myCardPrefab);

						myCardPrefab.GetComponent<TweenPosition> ().delay = i * 0.3f + 1;
						myCardPrefab.GetComponent<TweenPosition> ().duration = 1;
						myCardPrefab.GetComponent<TweenPosition> ().from = new Vector3 (-1200, 0, 0);
						myCardPrefab.GetComponent<TweenPosition> ().to = new Vector3 (-390f + (260 * i), -30f, 0f);
						myCardPrefab.GetComponent<TweenPosition> ().PlayForward ();

						int cardOnDeck = 0;
						string[] userCard = null;

						if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
								cardOnDeck = IntParseFast (partida.CartasJogador [i].ToString ());
						} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
								cardOnDeck = IntParseFast (partida.CartasAdversario [i].ToString ());
						}

						for (int j = 0; j < CardDeck.deck.Count; j++) {
								if (IntParseFast (CardDeck.deck [j] [13]) == cardOnDeck) {
										userCard = CardDeck.deck [j];
								}
						}

						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = userCard [12];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = userCard [1] + userCard [2];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Card_Card").FindChild ("Label_CardName").GetComponent<UILabel> ().text = userCard [0];

						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Potencia").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [4];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Torque").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [5];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Consumo").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [6];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Ruido").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [7];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Aceleracao").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [8];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Frenagem").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [9];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_Retomada").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [10];
						myCardPrefab.transform.FindChild ("Card").FindChild ("Sprite_Category_PortaMalas").FindChild ("Label_Points").GetComponent<UILabel> ().text = userCard [11];
				}
		}


		public void NextStep ()
		{
				//ChangeLayoutToNextStep ();
		}


		public void CheckCardCategory (int categoryNumber, int categoryValue, GameObject categoryObject)
		{
				//print (categoryObject.transform.parent.parent.name);

				if (categoryObject.transform.parent.parent.name == "CardContainer01") {
						print ("CardContainer01: " + categoryNumber + "/" + categoryValue);
						gameCardsCategoryNumberSelected [0] = categoryNumber;
						gameCardsCategoryValueSelected [0] = categoryValue;
				}
				if (categoryObject.transform.parent.parent.name == "CardContainer02") {
						print ("CardContainer02: " + categoryNumber + "/" + categoryValue);
						gameCardsCategoryNumberSelected [1] = categoryNumber;
						gameCardsCategoryValueSelected [1] = categoryValue;
				}
				if (categoryObject.transform.parent.parent.name == "CardContainer03") {
						print ("CardContainer03: " + categoryNumber + "/" + categoryValue);
						gameCardsCategoryNumberSelected [2] = categoryNumber;
						gameCardsCategoryValueSelected [2] = categoryValue;
				}
				if (categoryObject.transform.parent.parent.name == "CardContainer04") {
						print ("CardContainer04: " + categoryNumber + "/" + categoryValue);
						gameCardsCategoryNumberSelected [3] = categoryNumber;
						gameCardsCategoryValueSelected [3] = categoryValue;
				}
		}

		public void UpdateCurrentPartida ()
		{
				Partida partida = GameController.Partida;

				if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
						partida.MaoJogador = partida.CartasJogador.GetRange (0, 4);
						partida.MaoAdversario = null;
				} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
						partida.MaoAdversario = partida.CartasAdversario.GetRange (0, 4);
						partida.MaoJogador = null;
				}

				partida.CategoriaCarta1 = gameCardsCategoryNumberSelected [0];
				partida.CategoriaCarta2 = gameCardsCategoryNumberSelected [1];
				partida.CategoriaCarta3 = gameCardsCategoryNumberSelected [2];
				partida.CategoriaCarta4 = gameCardsCategoryNumberSelected [3];

				partida.StatusPartida = Partida.STATUS_JOGANDO;
				partida.DatePartida = System.DateTime.Now;

				LoadingSystem.Instance.ShowLoading ();

				StartCoroutine (
						new PartidaDAO ().SaveOrUpdate (partida, () => {
								Application.LoadLevel ("04.NewGameCreatedScene");
						}));
		}

		public void NextScene ()
		{
				LoadingSystem.Instance.ShowLoading ();

				Partida partida = GameController.Partida;

				if (partida != null) {
						if (partida.StatusPartida == Partida.STATUS_JOGANDO) {
								UpdateCurrentPartida ();
								return;
						}
				}

				string facebookIdADV = GameManager.tempChallengedFriendID;
				string facebookNomeCompletoADV = GameManager.tempChallengedFriendName;
				string facebookUrlADV = GameManager.tempChallengedFriendPhotoURL;

				StartCoroutine (jDAO.LoadByFacebookId (facebookIdADV, jogadores => {
						if (jogadores != null && jogadores.Count > 0) {
								CreatePartida (jogadores [0]);
						} else {
								CasoAdversarioNaoEncontrado (facebookNomeCompletoADV, facebookUrlADV);
						}
				}));
		}

	private void CasoAdversarioNaoEncontrado (string facebookNomeCompletoADV, string facebookUrlADV)
	{
		StartCoroutine (jDAO.LoadByNome (facebookNomeCompletoADV, jogadores => {
			if (jogadores != null && jogadores.Count > 0) {
				CreatePartida (jogadores [0]);
			} else {
				Jogador adversario = new Jogador ();
				adversario.Nome = facebookNomeCompletoADV;
				adversario.FotoURL = facebookUrlADV;
				
				SalvarNovoJogador (adversario);
			}
		}));
	}

		private void SalvarNovoJogador (Jogador adversario)
		{
				StartCoroutine (jDAO.SaveOrUpdate (adversario, () => {
						CreatePartida (adversario);
				}));
		}

		private void CreatePartida (Jogador adversario)
		{
				Partida partida = new Partida ();
				partida.Jogador = GameController.Jogador;
				partida.Adversario = adversario;

				partida.Jogador.Nome = GameController.Jogador.Nome;
				partida.Adversario.Nome = adversario.Nome;

				partida.CartasJogador = GameManager.tempUserCards;
				partida.CartasAdversario = GameManager.tempFriendCards;

				partida.MaoJogador = GameManager.tempUserHandCards;
				partida.MaoAdversario = null;

				partida.MaoJogadorAnterior = partida.MaoJogador;
				partida.MaoAdversarioAnterior = null;

				partida.CategoriaCarta1 = gameCardsCategoryNumberSelected [0]; 
				partida.CategoriaCarta2 = gameCardsCategoryNumberSelected [1]; 
				partida.CategoriaCarta3 = gameCardsCategoryNumberSelected [2]; 
				partida.CategoriaCarta4 = gameCardsCategoryNumberSelected [3]; 

				partida.CategoriaOld1 = partida.CategoriaCarta1;
				partida.CategoriaOld2 = partida.CategoriaCarta2;
				partida.CategoriaOld3 = partida.CategoriaCarta3;
				partida.CategoriaOld4 = partida.CategoriaCarta4;

				partida.Rodada = 1;

				partida.DatePartida = DateTime.Now;
				partida.StatusPartida = Partida.STATUS_ABERTO;
				partida.Turno = Partida.TURNO_ADVERSARIO;

				//Alterado fluxo na remoção das cartas do db
				StartCoroutine (pDAO.SaveOrUpdate (partida, () => {
						print ("Partida criada com sucesso");

						GameController.Partidas.Add (partida);
						GameController.Partida = partida;

						//Load Scene
						Application.LoadLevel ("04.NewGameCreatedScene");
				}));
		}

		public void PreviousScene ()
		{
				Application.LoadLevel ("02.GameMenuScene");
		}

		public static int IntParseFast (string value)
		{
				int result = 0;
				for (int i = 0; i < value.Length; i++) {
						char letter = value [i];
						result = 10 * result + (letter - 48);
				}
				return result;
		}
				
}
