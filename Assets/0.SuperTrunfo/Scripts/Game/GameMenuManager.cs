﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameMenuManager : MonoBehaviour
{
		public GamePrefab gamePreFab;
		public UILabel newGamesLabelTitle;
		public UILabel newGamesLabel;
		public List<GamePrefab> runningGamesList = new List<GamePrefab> ();
		public List<GamePrefab> newGamesList = new List<GamePrefab> ();
		public FriendSelectManager friendSelectManager;
		public UIScrollView scrollViewGames;

		//Parse.com
		private JogadorDAO jDAO = new JogadorDAO ();
		private PartidaDAO pDAO = new PartidaDAO ();
		private int runningRoutines = 0;
	
		// Use this for initialization
		void Start ()
		{
				Debug.Log ("[Super Trunfo]: Loading GameMenuScene");
				if (!GameManager.isLocal) {
						AudioManager.Instance.CheckSceneAndUpdate ();
						StartLoadingParse ();
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		void CreateGames ()
		{
				int numberOfGames = GameController.Partidas.Count;
				newGamesLabelTitle.text = "Suas partidas";

				if (numberOfGames > 0) {
						newGamesLabel.text = "Você possui novos jogos!";
				} else {
						newGamesLabel.text = "Você não possui nenhum jogo em andamento!";
				}

				
				foreach (Partida partida in GameController.Partidas) {
						string nomeJogador = partida.Jogador.Nome;
						string nomeAdversario = partida.Adversario.Nome;

						Vector3 gamePreFabPosition = new Vector3 (0, 0, 0);
						Vector3 gamePreFabScale = new Vector3 (1, 1, 1);

						GamePrefab myNewGamePrefab = Instantiate (gamePreFab, gamePreFabPosition, Quaternion.identity) as GamePrefab;

						myNewGamePrefab.gameTime = Random.Range (0.0F, 10.0F);

						myNewGamePrefab.transform.parent = GameObject.Find ("Panel_GamesAvaiable").transform;
						myNewGamePrefab.transform.localScale = gamePreFabScale;

						GameObject buttonPlayCurrentGame = myNewGamePrefab.transform.FindChild ("Button_PlayCurrentGame").gameObject;
						GameObject buttonRefuseCurrentGame = myNewGamePrefab.transform.FindChild ("Button_RefuseGame").gameObject;

						UILabel userCardsLabel = myNewGamePrefab.transform.FindChild ("Label_MyScore").GetComponent<UILabel> ();
						UILabel friendCardsLabel = myNewGamePrefab.transform.FindChild ("Label_FriendScore").GetComponent<UILabel> ();

						string userScore;
						string friendScore;
						
						int partidaMonth = partida.DatePartida.Month;
						int partidaDay = partida.DatePartida.Day;
						int partidaHour = partida.DatePartida.Hour;
						int partidaMinutes = partida.DatePartida.Minute;

						int currentMonth = System.DateTime.Now.Month;
						int currentDay = System.DateTime.Now.Day;
						int currentHour = System.DateTime.Now.Hour;
						int currentMinutes = System.DateTime.Now.Minute;

						string activeSince = "0 minutos atrás";
						
						if (currentMonth == partidaMonth) {
								if (currentDay == partidaDay) {
										if (currentHour == partidaHour) {
												if (currentMinutes >= partidaMinutes) {
														if ((currentMinutes - partidaMinutes) < 10) {
																if ((currentMinutes - partidaMinutes) <= 1) {
																		activeSince = "01 minuto atrás";
																} else {
																		activeSince = "0" + (currentMinutes - partidaMinutes) + " minutos atrás";
																}

														} else {
																activeSince = (currentMinutes - partidaMinutes) + " minutos atrás";
														}
												} else {
														activeSince = "1h atrás";
												}
										}

										if (currentHour > partidaHour) {
												activeSince = (currentHour - partidaHour) + "h atrás";
										}
								} else if (currentDay == partidaDay) {
										if (currentHour > partidaHour) {
												activeSince = (currentHour - partidaHour) + "h atrás";
										}
								} else if (currentDay > partidaDay) {
										activeSince = (currentDay - partidaDay) + " dias atrás";
								} else {
										activeSince = "+ de 30 dias";
								}
						} else {
								activeSince = "+ de 30 dias";
						}

						UILabel activeSinceLabel = myNewGamePrefab.transform.FindChild ("Label_WaitingSince").GetComponent<UILabel> ();
						activeSinceLabel.text = activeSince;


						if (partida.CartasJogador.Count < 10) {
								userScore = "0" + partida.CartasJogador.Count.ToString ();
						} else {
								userScore = partida.CartasJogador.Count.ToString ();
						}

						if (partida.CartasAdversario.Count < 10) {
								friendScore = "0" + partida.CartasAdversario.Count.ToString ();
						} else {
								friendScore = partida.CartasAdversario.Count.ToString ();
						}



						UIButton btn = buttonPlayCurrentGame.GetComponent<UIButton> ();
						EventDelegate del = new EventDelegate (this, "PlayCurrentGame");
						del.parameters [0].value = partida;
						EventDelegate.Set (btn.onClick, del);

						UIButton btn2 = buttonRefuseCurrentGame.GetComponent<UIButton> ();
						EventDelegate del2 = new EventDelegate (this, "OpenRefuseFeedback");
						del2.parameters [0].value = partida;
						del2.parameters [1].value = buttonRefuseCurrentGame;
						del2.parameters [2].value = buttonPlayCurrentGame;
						EventDelegate.Set (btn2.onClick, del2);

						//IF aguardando
						if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
								myNewGamePrefab.myNameSprite.GetComponent<UILabel> ().text = nomeJogador;			// Esquerda
								myNewGamePrefab.friendNameSprite.GetComponent<UILabel> ().text = nomeAdversario; 	// Direita
								userCardsLabel.text = userScore;
								friendCardsLabel.text = friendScore;

								GameManager.tempUserHandCards = partida.MaoJogador;
								GameManager.tempFriendHandCards = partida.MaoAdversario;

								GameManager.tempUserCards = partida.CartasJogador;
								GameManager.tempFriendCards = partida.CartasAdversario;

								GameManager.DownloadImage (partida.Jogador.FotoURL, myNewGamePrefab.photoTexture);
								GameManager.DownloadImage (partida.Adversario.FotoURL, myNewGamePrefab.friendPhotoTexture);

								if (partida.Turno == Partida.TURNO_JOGADOR) {
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().text = "JOGAR";
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().fontSize = 18;
								}

								if (partida.Turno == Partida.TURNO_ADVERSARIO) {
										buttonPlayCurrentGame.GetComponent<UIButton> ().isEnabled = false;
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().text = "AGUARDANDO";
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().fontSize = 15;
								}

								if (partida.StatusPartida == Partida.STATUS_FECHADO) {
										buttonPlayCurrentGame.GetComponent<UIButton> ().isEnabled = false;
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().text = "JOGO ENCERRADO";
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().fontSize = 16;
								}

								if (partida.StatusPartida == Partida.STATUS_FECHADO && partida.Turno == Partida.TURNO_ADVERSARIO) {
										buttonPlayCurrentGame.GetComponent<UIButton> ().isEnabled = true;
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().text = "JOGAR";
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().fontSize = 18;	
								}

								if (partida.StatusPartida == Partida.STATUS_ABERTO || partida.Turno == Partida.TURNO_JOGADOR) {
										myNewGamePrefab.isNewGame = true;
										newGamesList.Add (myNewGamePrefab);
								} else {
										runningGamesList.Add (myNewGamePrefab);
								}
						} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
								myNewGamePrefab.myNameSprite.GetComponent<UILabel> ().text = nomeAdversario;	// Direita
								myNewGamePrefab.friendNameSprite.GetComponent<UILabel> ().text = nomeJogador; 	// Esquerda
								userCardsLabel.text = friendScore;
								friendCardsLabel.text = userScore;

								GameManager.tempUserHandCards = partida.MaoAdversario;
								GameManager.tempFriendHandCards = partida.MaoJogador;

								GameManager.tempUserCards = partida.CartasAdversario;
								GameManager.tempFriendCards = partida.CartasJogador;

								GameManager.DownloadImage (partida.Jogador.FotoURL, myNewGamePrefab.friendPhotoTexture);
								GameManager.DownloadImage (partida.Adversario.FotoURL, myNewGamePrefab.photoTexture);

								if (partida.Turno == Partida.TURNO_ADVERSARIO) {
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().text = "JOGAR";
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().fontSize = 18;
								}

								if (partida.Turno == Partida.TURNO_JOGADOR) {
										buttonPlayCurrentGame.GetComponent<UIButton> ().isEnabled = false;
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().text = "AGUARDANDO";
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().fontSize = 18;
								}

								if (partida.StatusPartida == Partida.STATUS_FECHADO) {
										buttonPlayCurrentGame.GetComponent<UIButton> ().isEnabled = false;
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().text = "JOGO ENCERRADO";
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().fontSize = 18;
								}

								if (partida.StatusPartida == Partida.STATUS_FECHADO && partida.Turno == Partida.TURNO_JOGADOR) {
										buttonPlayCurrentGame.GetComponent<UIButton> ().isEnabled = true;
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().text = "JOGAR";
										buttonPlayCurrentGame.GetComponentInChildren<UILabel> ().fontSize = 18;
								}

								if (partida.StatusPartida == Partida.STATUS_ABERTO || partida.Turno == Partida.TURNO_ADVERSARIO) {
										myNewGamePrefab.isNewGame = true;
										newGamesList.Add (myNewGamePrefab);
								} else {
										runningGamesList.Add (myNewGamePrefab);
								}
						}
				}
				
				scrollViewGames.ResetPosition ();
		}

		void OrderGames ()
		{
				runningGamesList = runningGamesList.OrderBy (go => go.gameTime).ToList ();
				newGamesList = newGamesList.OrderBy (go => go.gameTime).ToList ();

				int i;

				for (i = 0; i < newGamesList.Count; i++) {
						Vector3 newGamesPosition = new Vector3 (0, 180 - (60 * i), 0);
						newGamesList [i].transform.localPosition = newGamesPosition;
				}
						
				for (int j = 0; j < runningGamesList.Count; j++) {
						Vector3 allGamesPosition = new Vector3 (0, 180 - (60 * i), 0);
						runningGamesList [j].transform.localPosition = allGamesPosition;
						i++;
				}
		}

		public void ChallengeCurrentFriend ()
		{
				Application.LoadLevel ("03.NewGameScene");
		}

		public void PlayCurrentGame (Partida currentPartida)
		{
				GameController.Partida = currentPartida as Partida;
				//Partida partida = GameController.Partida;


				if (currentPartida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
						if (currentPartida.StatusPartida == Partida.STATUS_ABERTO) {
								print ("Entrou aqui0");
								GameManager.challengeTime = true;
								Application.LoadLevel ("05.MyTurnScene");

						} else if (currentPartida.StatusPartida == Partida.STATUS_JOGANDO && currentPartida.Turno == Partida.TURNO_JOGADOR && currentPartida.MaoJogador == null && currentPartida.MaoAdversario == null) {
								print ("Entrou aqui1");
								GameManager.challengeTime = true;
								Application.LoadLevel ("03.NewGameScene");

						} else if (currentPartida.StatusPartida == Partida.STATUS_JOGANDO && currentPartida.Turno == Partida.TURNO_JOGADOR && currentPartida.MaoJogador == null && currentPartida.MaoAdversario != null) {
								print ("Entrou aqui2");
								GameManager.challengeTime = false;
								Application.LoadLevel ("06.CardBattleScene");

						} else if (currentPartida.StatusPartida == Partida.STATUS_JOGANDO && currentPartida.Turno == Partida.TURNO_JOGADOR && currentPartida.MaoJogador != null && currentPartida.MaoAdversario != null) {
								print ("Entrou aqui3");
								GameManager.challengeTime = true;
								Application.LoadLevel ("05.MyTurnScene");
						}

				} else if (currentPartida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
						if (currentPartida.StatusPartida == Partida.STATUS_ABERTO) {
								print ("Entrou aqui4");
								GameManager.challengeTime = true;
								Application.LoadLevel ("05.MyTurnScene");

						} else if (currentPartida.StatusPartida == Partida.STATUS_JOGANDO && currentPartida.Turno == Partida.TURNO_ADVERSARIO && currentPartida.MaoAdversario == null && currentPartida.MaoJogador == null) {
								print ("Entrou aqui5");
								GameManager.challengeTime = true;
								Application.LoadLevel ("03.NewGameScene");

						} else if (currentPartida.StatusPartida == Partida.STATUS_JOGANDO && currentPartida.Turno == Partida.TURNO_ADVERSARIO && currentPartida.MaoAdversario == null && currentPartida.MaoJogador != null) {
								print ("Entrou aqui6");
								GameManager.challengeTime = false;
								Application.LoadLevel ("06.CardBattleScene");

						} else if (currentPartida.StatusPartida == Partida.STATUS_JOGANDO && currentPartida.Turno == Partida.TURNO_ADVERSARIO && currentPartida.MaoAdversario != null && currentPartida.MaoJogador != null) {
								print ("Entrou aqui7");
								GameManager.challengeTime = true;
								Application.LoadLevel ("05.MyTurnScene");

						} else if (currentPartida.StatusPartida == Partida.STATUS_FECHADO) {
								print ("Entrou aqui8");
								GameManager.challengeTime = false;
								Application.LoadLevel ("06.CardBattleScene");
						}
				}




		}

		public void PreviouScene ()
		{
				Application.LoadLevel ("01.MainScene");
		}
				
		private void StartLoadingParse ()
		{
				LoadingSystem.Instance.ShowLoading ();
		
				string facebookId = GameManager.jogadorUserID; 
				string facebookNomeCompleto = GameManager.jogadorUserName;
				string facebookFotoURL = GameManager.jogadorUserPhotoURL; 
		
				StartCoroutine (jDAO.LoadByFacebookId (facebookId, jogadores => {
						if (jogadores != null && jogadores.Count > 0) {
								LoadPartidas (jogadores [0]);
						} else {
								CasoPrimeiroAcesso (facebookId, facebookNomeCompleto, facebookFotoURL);
						}
				}));
		}

		private void CasoPrimeiroAcesso (string facebookId, string facebookNomeCompleto, string facebookFotoURL)
		{
				StartCoroutine (jDAO.LoadByNome (facebookNomeCompleto, jogadores => {
						if (jogadores != null && jogadores.Count > 0) {
								jogadores [0].FaceBookId = facebookId;
								SalvarJogador (jogadores [0]);
						} else {
								Jogador jogador = new Jogador ();
								jogador.FaceBookId = facebookId;
								jogador.Nome = facebookNomeCompleto;
								jogador.FotoURL = facebookFotoURL;
				
								SalvarJogador (jogador);
						}
				}));
		}

		private void SalvarJogador (Jogador jogador)
		{
				StartCoroutine (jDAO.SaveOrUpdate (jogador, () => {
						//Load Scene
						LoadPartidas (jogador);
				}));
		}

		private void LoadPartidas (Jogador jogador)
		{
				GameController.Jogador = jogador;
				StartCoroutine (pDAO.LoadByJogador (jogador, partidas => {
						GameController.Partidas = new List<Partida> ();
						LoadPlayerNamesAndURL (partidas);
				}));
		}

		private void LoadPlayerNamesAndURL (List<Partida> partidas)
		{
				foreach (Partida partida in partidas) {
						runningRoutines++;
						runningRoutines++;
			
						StartCoroutine (jDAO.LoadByObjectId (partida.Jogador.ObjectId, jogador => {
								foreach (Partida p in GameController.Partidas) {
										if (p.Jogador.ObjectId == jogador.ObjectId) {
												p.Jogador.Nome = jogador.Nome;
												p.Jogador.FotoURL = jogador.FotoURL;
												p.Jogador.FaceBookId = jogador.FaceBookId;
										}
								}
								runningRoutines--;
						}));
			
						StartCoroutine (jDAO.LoadByObjectId (partida.Adversario.ObjectId, adversario => {
								foreach (Partida p in GameController.Partidas) {
										if (p.Adversario.ObjectId == adversario.ObjectId) {
												p.Adversario.Nome = adversario.Nome;
												p.Adversario.FotoURL = adversario.FotoURL;
												p.Adversario.FaceBookId = adversario.FaceBookId;
										}
								}
								runningRoutines--;
						}));
			
						GameController.Partidas.Add (partida);
				}
		
				StartCoroutine (CheckStartRoutines ());
		}

		private IEnumerator CheckStartRoutines ()
		{
				bool flag = true;
				while (flag) {
						if (runningRoutines == 0) {
								RemoveFriendsFromListIfPlayerHasGame ();
								flag = false;
						}
						yield return new WaitForSeconds (0.1f);
				}
		}

		private void RemoveFriendsFromListIfPlayerHasGame ()
		{
				GameManager.friendsList.Clear ();
				GameManager.friendsList = new List<object> (GameManager.friendsListOriginal);

				foreach (Partida partida in GameController.Partidas) {
						for (int i = 0; i < GameManager.friendsList.Count; i++) {
								if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
										if (GameManager.GetFriendsNames (i).ToString () == partida.Adversario.Nome) {
												print (GameManager.GetFriendsNames (i) + " já existe, removendo da lista de amigos disponíveis...");
												GameManager.friendsList.RemoveAt (i);
										}
								} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
										if (GameManager.GetFriendsNames (i).ToString () == partida.Jogador.Nome) {
												print (GameManager.GetFriendsNames (i) + " já existe, removendo da lista de amigos disponíveis...");
												GameManager.friendsList.RemoveAt (i);
										}
								}
						}
				}

				Debug.Log (GameManager.friendsList.Count);
				Debug.Log (GameManager.friendsListOriginal.Count);

				LoadingSystem.Instance.CloseLoading ();
				//SceneManager.GoToMainScene ();
				friendSelectManager.PopulateWithFriends ();
				CreateGames ();
				OrderGames ();
		}

		public UIPanel refuseFeedback;
		public Partida currentPartidaToDelete;
		public GameObject go1ToDelete;
		public GameObject go2ToDelete;

		public void OpenRefuseFeedback (Partida partida, GameObject go1, GameObject go2)
		{
				refuseFeedback.transform.localPosition = new Vector3 (0, 0, 0);

				currentPartidaToDelete = partida;
				go1ToDelete = go1;
				go2ToDelete = go2;
		}

		public void CloseRefuseFeedback ()
		{
				refuseFeedback.transform.localPosition = new Vector3 (-3000, 0, 0);
		}

		public void ConfirmRefuseFeedback (Partida partida, GameObject go1, GameObject go2)
		{
				RefusePartida (partida, go1, go2);
				refuseFeedback.transform.localPosition = new Vector3 (-3000, 0, 0);
		}

		public void RefusePartida (Partida partida, GameObject go1, GameObject go2)
		{
				LoadingSystem.Instance.ShowLoading ();
				//go1.GetComponentInChildren<UILabel> ().text = "REMOVENDO...";
				//go1.GetComponent<UIButton>().isEnabled = false;
				//NGUITools.SetActive (go2, false);
				go2.GetComponentInChildren<UILabel> ().text = "REMOVENDO...";
				go2.GetComponent<UIButton> ().isEnabled = false;
				StartCoroutine (
						new PartidaDAO ().Delete (partida, () => {
						GameController.Partidas.Remove (partida);
						GameController.Partida = null;
						Application.LoadLevel ("02.GameMenuScene");
				}));
		}
}


