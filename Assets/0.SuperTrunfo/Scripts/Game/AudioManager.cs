﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public static AudioManager Instance;
	public AudioClip backgroundMusic;
	public AudioClip fightMusic;

	public bool isPlayingLoop;

	void Awake ()
	{
		Instance = this;
	}
	
	// Use this for initialization
	void Start() {
		DontDestroyOnLoad(this);
		AudioSource audio = GetComponent<AudioSource>();
		audio.clip = backgroundMusic;
		audio.loop = true;
		isPlayingLoop = true;
		audio.Play();

		//MuteAll();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CheckSceneAndUpdate(){
		AudioSource audio = GetComponent<AudioSource>();
		if(Application.loadedLevel == 0 ||
		   Application.loadedLevel == 1 ||
		   Application.loadedLevel == 2 ||
		   Application.loadedLevel == 3 ||
		   Application.loadedLevel == 4 ||
		   Application.loadedLevel == 5 ||
		   Application.loadedLevel == 7 ||
		   Application.loadedLevel == 10){
			audio.clip = backgroundMusic;
		}else{
			audio.clip = fightMusic;
			isPlayingLoop = false;
		}
		audio.loop = true;

		if(!isPlayingLoop){
			audio.Play();
		}
	}

	public void MuteAll(){
		AudioListener.volume = 0;
	}
	public void UnMuteAll(){
		AudioListener.volume = 1;
	}
}
