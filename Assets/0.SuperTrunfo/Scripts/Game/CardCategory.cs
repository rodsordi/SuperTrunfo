﻿using UnityEngine;
using System.Collections;

public class CardCategory : MonoBehaviour
{

		public CardContainer cardContainer;
		public string categoryLabel;
		public int categoryValue;

		// Use this for initialization
		void Start ()
		{
				//categoryValue = Random.Range (0, 500);
				//transform.FindChild ("Label_Points").GetComponent<UILabel> ().text = categoryValue.ToString ();
		if (transform.parent.FindChild ("TipContainer")) {
			GameObject go = transform.parent.FindChild ("TipContainer").gameObject;
			NGUITools.SetActive (go, false);
		}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

	void OnHover (bool isOver)
	{
		if (transform.parent.FindChild ("TipContainer")) {
		GameObject go = transform.parent.FindChild ("TipContainer").gameObject;
		CheckStatus (go);
		Vector3 currentGoVector3 = new Vector3 (transform.localPosition.x, transform.localPosition.y + 12, transform.localPosition.z);
		
		if (isOver) {
			NGUITools.SetActive (go, true);
			go.transform.localPosition = currentGoVector3;
			//go.GetComponent<iTweenEvent> ().Play ();
		} else {
			NGUITools.SetActive (go, false);
		}
		}
		
	}
	
	void CheckStatus (GameObject go)
	{
		go.transform.FindChild ("CategoriaLabel").GetComponent<UILabel> ().text = categoryLabel;
		
		if (categoryLabel == "Potência") {
			go.transform.FindChild ("DescriptionLabel").GetComponent<UILabel> ().text = "Força disponível para movimentar o veículo.";
			go.transform.FindChild ("UnitLabel").GetComponent<UILabel> ().text = "Unidade: cv (cavalo-vapor)";
		}
		
		if (categoryLabel == "Torque") {
			go.transform.FindChild ("DescriptionLabel").GetComponent<UILabel> ().text = "Força absoluta do motor.";
			go.transform.FindChild ("UnitLabel").GetComponent<UILabel> ().text = "Unidade: mkgf";
		}
		
		if (categoryLabel == "Cons. urb") {
			go.transform.FindChild ("DescriptionLabel").GetComponent<UILabel> ().text = "Quantidade de quilômetros que o veículo pode rodar gastando um litro de combustível.";
			go.transform.FindChild ("UnitLabel").GetComponent<UILabel> ().text = "Unidade: km/l";
		}
		
		if (categoryLabel == "Ruído p.m") {
			go.transform.FindChild ("DescriptionLabel").GetComponent<UILabel> ().text = "Intensidade do som produzido pelo motor em ponto morto.";
			go.transform.FindChild ("UnitLabel").GetComponent<UILabel> ().text = "Unidade: db (decíbeis)";
		}
		
		if (categoryLabel == "Aceleração") {
			go.transform.FindChild ("DescriptionLabel").GetComponent<UILabel> ().text = "Tempo necessário para o veículo sair da imobilidade e atingir a marca dos 100 km/h.";
			go.transform.FindChild ("UnitLabel").GetComponent<UILabel> ().text = "Unidade: segundos";
		}
		
		if (categoryLabel == "Fren. 120-0") {
			go.transform.FindChild ("DescriptionLabel").GetComponent<UILabel> ().text = "Distância necessária para que o veículo pare completamente viajando a 120 km/h.";
			go.transform.FindChild ("UnitLabel").GetComponent<UILabel> ().text = "Unidade: metros";
		}
		
		if (categoryLabel == "Retom. 40-80") {
			go.transform.FindChild ("DescriptionLabel").GetComponent<UILabel> ().text = "Tempo necessário para o veículo recuperar a velocidade, partindo de 40 km/h até atingir os 80 km/h.";
			go.transform.FindChild ("UnitLabel").GetComponent<UILabel> ().text = "Unidade: segundos";
		}
		
		if (categoryLabel == "Porta-malas") {
			go.transform.FindChild ("DescriptionLabel").GetComponent<UILabel> ().text = "Capacidade volumétrica de carga para transporte. Quanto maior o volume, mais bagagem o carro poderá levar.";
			go.transform.FindChild ("UnitLabel").GetComponent<UILabel> ().text = "Unidade: litros";
		}
		
		if (categoryLabel == "Potência" ||
		    categoryLabel == "Torque" ||
		    categoryLabel == "Cons. urb" ||
		    categoryLabel == "Porta-malas") {
			go.transform.FindChild ("WinsLabel").GetComponent<UILabel> ().text = "Ganha o MAIOR valor";
		} else {
			go.transform.FindChild ("WinsLabel").GetComponent<UILabel> ().text = "Ganha o MENOR valor";
		}
		
	}
}
