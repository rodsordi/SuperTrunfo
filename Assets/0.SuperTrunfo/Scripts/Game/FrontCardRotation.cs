﻿using UnityEngine;
using System.Collections;

public class FrontCardRotation : MonoBehaviour
{

		public GameObject cardParent;
		bool alreadyRotate;

		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
				CheckRotation ();
		}

		void CheckRotation ()
		{
				if (cardParent.transform.localRotation.y <= -0.7 && !alreadyRotate) {
						gameObject.GetComponent<UISprite> ().enabled = false;
						alreadyRotate = true;
				}

				if (cardParent.transform.localRotation.y == 0 && alreadyRotate) {
						gameObject.GetComponent<UISprite> ().enabled = true;
						alreadyRotate = false;
				}
		}
}
