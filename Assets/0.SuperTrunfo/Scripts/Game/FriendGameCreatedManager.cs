﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class FriendGameCreatedManager : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
				Partida partida = GameController.Partida;
				if (partida.StatusPartida == Partida.STATUS_JOGANDO) {
						SendMessageToFriend ();
				} else {
						NewGameChallengeFeed ();
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void GoToMainScene ()
		{
			Application.LoadLevel ("01.MainScene");
		}

		public void GoToMenuScene ()
		{
				Application.LoadLevel ("02.GameMenuScene");
		}

		private void NewGameChallengeFeed ()
		{
				string[] recipient = { GameManager.tempChallengedFriendID };
				FB.AppRequest (
			message: "Te desafio para o Duelo Quatro Rodas! Vamos ver quem entende mais de carros?",
			to: recipient,
			filters : null,
			excludeIds : null,
			maxRecipients : null,
			data: null,
			title: "Novo Desafio Quatro Rodas!",
			callback: appCallFriendRequest
				);
		}

		public void SendMessageToFriend ()
		{
				Partida partida = GameController.Partida;
				string template = null;
				string query = null;

				if (partida.Jogador.ObjectId == GameController.Jogador.ObjectId) {
					template = partida.Jogador.Nome + " já escolheu as cartas! Agora é a sua vez, vamos lá? Clique aqui para jogar!";
					query = "/v2.3/" + partida.Adversario.FaceBookId + "/notifications?" + GameManager.gameAppToken;
				} else if (partida.Adversario.ObjectId == GameController.Jogador.ObjectId) {
					template = partida.Adversario.Nome + " já escolheu as cartas! Agora é a sua vez, vamos lá? Clique aqui para jogar!";
					query = "/v2.3/" + partida.Jogador.FaceBookId + "/notifications?" + GameManager.gameAppToken;	
				}

				Dictionary<string, string> notificationData = new Dictionary<string, string> ();
				notificationData.Add("template", template);
		
				print ("Calling API");
				CallAPI(query, template, notificationData);
		}

		private void appCallFriendRequest (FBResult result)
		{
				Debug.Log ("appCallFriendRequest");
				if (result != null) {
						var responseObject = Json.Deserialize (result.Text) as Dictionary<string, object>;
						object obj = 0;
						if (responseObject.TryGetValue ("cancelled", out obj)) {
								Util.Log ("Request cancelled");
						} else if (responseObject.TryGetValue ("request", out obj)) {
								Util.Log ("Request sent");
						}
				}
		}

		private void CallAPI(string query, string template, Dictionary<string, string> notificationData){
			FB.API (query, Facebook.HttpMethod.POST, appSendMessageToFriend, notificationData);
		}

		private void appSendMessageToFriend (FBResult result)
		{
				Debug.Log ("appSendMessageToFriend");
				Debug.Log(result.Text);
				if (result != null) {
					Debug.Log (result.Error);
					return;
				}
		}
}
