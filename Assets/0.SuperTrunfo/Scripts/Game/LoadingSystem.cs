﻿using UnityEngine;
using System.Collections;

public class LoadingSystem : MonoBehaviour
{

		public static LoadingSystem Instance;

		void Awake ()
		{
				Instance = this;
		}

		public void ShowLoading ()
		{
				Instance.gameObject.transform.FindChild ("Loading").GetComponent<UISprite> ().enabled = true;
				Instance.gameObject.transform.FindChild ("Loading").GetComponent<BoxCollider> ().enabled = true;
				Instance.gameObject.transform.FindChild ("Loading").FindChild ("Sprite").GetComponent<UISprite> ().enabled = true;
		}

		public void CloseLoading ()
		{
				Instance.gameObject.transform.FindChild ("Loading").GetComponent<UISprite> ().enabled = false;
				Instance.gameObject.transform.FindChild ("Loading").GetComponent<BoxCollider> ().enabled = false;
				Instance.gameObject.transform.FindChild ("Loading").FindChild ("Sprite").GetComponent<UISprite> ().enabled = false;
		}


}
