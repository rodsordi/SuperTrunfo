﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class FacebookManager : MonoBehaviour
{

		public GameObject playButton;
		public GameObject facebookLoginButton;
		string userQuery = "/v2.3/me?fields=id,name";
		string friendsQuery = "/v2.3/me?fields=friends,picture";
		string invitableFriendsQuery = "/v2.3/me/invitable_friends?limit=500";
		//private static List<object> friends = null;
		private Dictionary<string, string> profile = null;
		private Dictionary<string, string> profileID = null;


		//Parse.com
		//private JogadorDAO jDAO = new JogadorDAO ();
		//private PartidaDAO pDAO = new PartidaDAO ();
		//private int runningRoutines = 0;
	

		// Use this for initialization
		void Start ()
		{
				LoadingSystem.Instance.ShowLoading ();
				DontDestroyOnLoad (transform.gameObject);
				FB.Init (InitFacebook, OnHideUnity);
				//playButton.GetComponent<UIButton> ().isEnabled = false;
				//facebookLoginButton.GetComponent<UIButton> ().isEnabled = false;
		}

		private void OnHideUnity(bool isGameShown)
		{
			Util.Log("OnHideUnity");
			if (!isGameShown)
			{
				// pause the game - we will need to hide
				//Time.timeScale = 0;
			}
			else
			{
				// start the game back up - we're getting focus again
				//Time.timeScale = 1;
			}
		}

		// Update is called once per frame
		void Update ()
		{

		}

		void InitFacebook ()
		{
				Debug.Log ("[Super Trunfo]: Logging in");
				facebookLoginButton.GetComponent<UIButton> ().isEnabled = true;
				LoadingSystem.Instance.CloseLoading ();

				if (FB.IsLoggedIn) 
				{
					LoadingSystem.Instance.ShowLoading ();
					GetUserInfo ();
				}
		}

	
	public static int IntParseFast (string value)
	{
		int result = 0;
		for (int i = 0; i < value.Length; i++) {
			char letter = value [i];
			result = 10 * result + (letter - 48);
		}
		return result;
	}

		public void LoginWithFacebook ()
		{
				FB.Login ("public_profile,user_friends,email,publish_actions", LoginCallback);
		}

		void GetUserInfo ()
		{
				FB.API (userQuery, Facebook.HttpMethod.GET, UserQueryCallBack);
				GetAppToken();
		}

		void GetUserFriendsWhoPlay ()
		{
				FB.API (friendsQuery, Facebook.HttpMethod.GET, GetUserFriendWhoPlayCallBack);
		}

		void GetUserFriendList ()
		{
				FB.API (invitableFriendsQuery, Facebook.HttpMethod.GET, GetUserFriendListCallBack);
		}

		void GetAppToken(){

			FB.API ("/oauth/access_token?client_id=" + GameManager.mainAPPID + "&client_secret=" + GameManager.mainAPPSecret + "&grant_type=client_credentials", Facebook.HttpMethod.GET, GetAppTokenCallBack);
		}

		void GetAppTokenCallBack (FBResult result){
			GameManager.gameAppToken = result.Text;
		}


		//########################################################
		//CALLBACKS
		//########################################################

		void LoginCallback (FBResult result)
		{
				if(FB.IsLoggedIn){
					Debug.Log ("[Super Trunfo]: LoginCallBack");
					LoadingSystem.Instance.ShowLoading ();
					GetUserInfo ();
				}
		}

		void UserQueryCallBack (FBResult result)
		{
				Debug.Log ("[Super Trunfo]: UserQueryCallBack");

				profile = Util.DeserializeJSONProfile (result.Text);
				profileID = Util.DeserializeJSONProfileID (result.Text);

				GameManager.jogadorUserName = profile ["name"];
				GameManager.jogadorUserID = profileID ["id"];
				LoadPictureAPI (Util.GetPictureURL (GameManager.jogadorUserID, 128, 128));

				GetUserFriendList ();
		}

		void LoadPictureAPI (string url)
		{
				FB.API (url, Facebook.HttpMethod.GET, result => {
						if (result.Error != null) {
								Util.LogError (result.Error);
								return;
						}
			
						GameManager.jogadorUserPhotoURL = Util.DeserializePictureURLString (result.Text);
				});
		}

		void GetUserFriendListCallBack (FBResult result)
		{
				Debug.Log ("Text " + result.Text);
				var dict = Json.Deserialize (result.Text) as Dictionary<string, object>;

				List<object> friends = dict ["data"] as List<object>;
				Dictionary<string, object> friendData = friends [0] as Dictionary<string, object>;
				Dictionary<string, object> friendPicture = friendData ["picture"] as Dictionary<string, object>;
				Dictionary<string, object> friendPictureData = friendPicture ["data"] as Dictionary<string, object>;

				//object friendName = friendData ["name"];
				//object friendURL = friendPictureData ["url"];

				Debug.Log ("Total friends: " + friends.Count);

				//GameManager.friendsTotal = friends.Count;

				GameManager.friendsList = friends;
				GameManager.friendData = friendData;
				GameManager.friendPictureData = friendPictureData;

				GetUserFriendsWhoPlay ();
		}

		void GetUserFriendWhoPlayCallBack (FBResult result)
		{
				List<object> friends = Util.DeserializeJSONFriends (result.Text);

				for (int i = 0; i < friends.Count; i++) {
						//Dictionary<string,object> friendsDict = friends [i] as Dictionary<string,object>;
						//object friendName = friendsDict ["name"];
						//object friendID = friendsDict ["id"];	
						GameManager.friendsList.Add (friends [i] as Dictionary<string,object>);
						GameManager.friendsTotal = GameManager.friendsList.Count;

						//GameManager.friendsList.Reverse ();
				}

				GameManager.friendsListOriginal = new List<object>(GameManager.friendsList);

				if (FB.IsLoggedIn) {
						//StartLoadingParse ();
						SceneManager.GoToMainScene ();
				}
						
				/*
				FB.API (Util.GetPictureURL (friendID.ToString (), 128, 128), Facebook.HttpMethod.GET, result2 => {
						if (result2.Error != null) {
								Util.LogError (result.Error);
								return;
						}

						//Debug.Log ("Amigo que joga: " + friendName);
						//Debug.Log ("Amigo que joga ID: " + friendID);
						//Debug.Log ("Amigo que joga foto: " + Util.DeserializePictureURLString (result2.Text));

						GameManager.friendsList.Add(friends [0] as Dictionary<string,object>);

						Debug.Log(GameManager.GetFriendsNames(GameManager.friendsList.Count - 1));
				});
				*/
		}
}

