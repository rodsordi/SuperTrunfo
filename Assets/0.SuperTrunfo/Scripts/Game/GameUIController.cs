﻿using UnityEngine;
using System.Collections;

public class GameUIController : MonoBehaviour {

	public GameObject helpButton;
	public GameObject soundButton;
	public GameObject fullScreenButton;

	bool soundActive = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (AudioListener.volume != 0) {
			soundButton.transform.FindChild("Background").GetComponent<UISprite> ().spriteName = "Button_SoundOn";
		} else {
			soundButton.transform.FindChild("Background").GetComponent<UISprite> ().spriteName = "Button_SoundOff";
		}
	}

	public void OnClickHelpButton(){
		//helpButton
	}

	public void OnClickSoundButton(){
		if (AudioListener.volume == 0) {
			soundActive = false;
		}

		soundActive = !soundActive;

		if (soundActive) {
			AudioManager.Instance.UnMuteAll();
		} else {
			AudioManager.Instance.MuteAll();
		}
	}

	public void OnClickFullScreenButton(){
		//fullScreenButton
	}
}
