﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class SharePhotoSceneManager : MonoBehaviour
{

	public GameObject carPrefab;
	public UITexture myPhoto;
	public string currentPhrase = "Eu só sei falar disso!";
	public GameObject alertPanel;

	void Awake(){
		CardDeck.ArrangeDeck();
		NGUITools.SetActive(alertPanel, false);
	}

	// Use this for initialization
	void Start ()
	{
		LoadCars();
		//LoadPhotosFromFacebook ();
	}

	// Update is called once per frame
	void Update ()
	{

	}

	void LoadCars(){
		int lines = 0;
		int columns = 0;

		for (int i = 0; i < CardDeck.deck.Count; i++) {
			Vector3 cardPrefabPosition = new Vector3 (0, 0, 0);
			Vector3 cardPrefabScale = new Vector3 (1, 1, 1);
			
			GameObject myCardPrefab = Instantiate (carPrefab, cardPrefabPosition, Quaternion.identity) as GameObject;
			myCardPrefab.name = CardDeck.deck[i] [13];
			
			myCardPrefab.transform.parent = GameObject.Find ("Scroll View").transform;
			myCardPrefab.transform.localScale = cardPrefabScale;
			myCardPrefab.transform.localPosition = new Vector3 (-400 + (lines * 266), 130 - (columns * 180), 0);

			UIButton btn = myCardPrefab.transform.FindChild("Sprite_CardPhoto").GetComponentInChildren<UIButton> ();
			EventDelegate del = new EventDelegate (this, "ShareThisCar");
			del.parameters [0].value = myCardPrefab;
			EventDelegate.Set (btn.onClick, del);
			
			lines++;
			if (lines >= 4) {
				lines = 0;
				columns++;
			}

			myCardPrefab.transform.FindChild ("Sprite_CardPhoto").GetComponent<UISprite> ().spriteName = CardDeck.deck [i][12];
			myCardPrefab.transform.FindChild ("Sprite_CardPhoto").FindChild ("Label_CardNumber").GetComponent<UILabel> ().text = CardDeck.deck[i] [1] + CardDeck.deck[i] [2];
			myCardPrefab.transform.FindChild ("Sprite_CardPhoto").FindChild ("Label_CardName").GetComponent<UILabel> ().text = CardDeck.deck[i] [0];
		}

		GameManager.DownloadImage (GameManager.jogadorUserPhotoURL, myPhoto);
	}



	public void ShareThisCar(GameObject car){
		LoadingSystem.Instance.ShowLoading();
		string newMessage = null;
		string newPhoto = null;

		for (int i = 0; i < CardDeck.deck.Count; i++) {
			if(car.name == CardDeck.deck[i][13]){
				newMessage = "Eu joguei o Duelo Quatro Rodas e meu carro preferido é o " + CardDeck.deck[i][0];
				newPhoto = GameManager.mainURL + "/carros/" + CardDeck.deck[i][3];
			}
		}


		FB.Feed (
			link: "http://apps.facebook.com/" + FB.AppId + "/?challenge_brag=" + (FB.IsLoggedIn ? FB.UserId : "guest"),
			linkName: currentPhrase,
			linkCaption: "Duelo Quatro Rodas",
			linkDescription: newMessage,
			picture: newPhoto,
			callback: ShareThisCarCallback	
			);
			
	}

	void ShareThisCarCallback(FBResult result){
		print(result.Text);
		var dict = Json.Deserialize (result.Text) as Dictionary<string, object>;

		try {
			object dictCancelled = dict ["cancelled"];
			if(dictCancelled.ToString() == "true"){

			}
		} catch (System.Exception ex) {
			Debug.Log(ex);
			ShowAlert();
		}
		LoadingSystem.Instance.CloseLoading();
	}

	public void ChangePhrase(string phrase){
		currentPhrase = phrase;
	}

	public void ShowAlert(){
		NGUITools.SetActive(alertPanel, true);
	}

	public void CloseAlert(){
		NGUITools.SetActive(alertPanel, false);
	}

	public void BackToMenu(){
		Application.LoadLevel("01.MainScene");
	}

	/*
	void LoadPhotosFromFacebook ()
	{
		string photoQuery = "/v2.3/me/photos/uploaded?fields=images&limit=5";
		
		FB.API (photoQuery, Facebook.HttpMethod.GET, PhotoQueryCallBack);
	}
	
	void PhotoQueryCallBack (FBResult result)
	{
		var dict = Json.Deserialize (result.Text) as Dictionary<string, object>;
		
		List<object> dataList = dict ["data"] as List<object>;								
		
		int lines = 0;
		int columns = 0;
		
		for (int i = 0; i < dataList.Count; i++) {
			Dictionary<string, object> imageData = dataList [i] as Dictionary<string, object>;	
			List<object> imageList = imageData ["images"] as List<object>;				
			Dictionary<string, object> imageDict = imageList [6] as Dictionary<string, object>;
			
			UITexture photoTexture = Instantiate (targetTexture) as UITexture;
			
			photoTexture.transform.parent = GameObject.Find ("Scroll View").transform;
			photoTexture.transform.localScale = new Vector3 (1,1,1);
			photoTexture.transform.localPosition = new Vector3 (-338 + (lines * 338), 220 - (columns * 220), 0);
			
			GameManager.DownloadImage (imageDict ["source"].ToString(), photoTexture);
			
			lines++;
			if (lines >= 3) {
				lines = 0;
				columns++;
			}
		}	
	}
	*/



}