﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

public class ParseDAO <T> where T : ParseObject {
	//private T Result;
	protected bool Log = false;

	public ParseDAO () {}

	public ParseDAO (bool log) {
		this.Log = log;
	}

	public void SaveOrUpdate(T po) {
		po.SaveAsync ().ContinueWith (t => {
			if (t.IsCompleted && Log) Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "SaveOrUpdate", "ObjectId = " + po.ObjectId));
			if (t.IsFaulted) Debug.LogException (t.Exception);
		});
	}

	public IEnumerator SaveOrUpdate(T po, Action action) {
		var task = po.SaveAsync ().ContinueWith (t => {
			if (t.IsCompleted && Log) Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "SaveOrUpdate", "ObjectId = " + po.ObjectId));
			if (t.IsFaulted) {
				LoadingSystem.Instance.CloseLoading();
				Debug.LogException (t.Exception);
			}
		});

		return CreateGameObject ().Save (task, action);
	}

	public void SaveOrUpdateList (List<T> list) {
		ParseObject.SaveAllAsync<T> (list);
	}

	public IEnumerator SaveOrUpdateList (List<T> poList, Action action) {
		var task = ParseObject.SaveAllAsync<T> (poList);
		return CreateGameObject ().Save (task, action); 
	}

	public IEnumerator Delete(T po, Action action) {
		var task = po.DeleteAsync ();
		return CreateGameObject ().Save (task, action);
	}

	public IEnumerator LoadByObjectId (string objectId, Action<T> action) {
		var query = new ParseQuery<T> ();
		var task = query.GetAsync(objectId.ToString());

		return CreateGameObject ().Load (task, action);
	}

	public IEnumerator LoadAll (Action<List <T>> action)
	{
		var query = new ParseQuery<T>();
		var task = query.FindAsync ();
		
		return CreateGameObject ().Load (task, action);
	}

	protected GameObjectDAO CreateGameObject () {
		GameObject go = new GameObject ("Parse.com: " + typeof(T).ToString () + " processing.");
		GameObjectDAO goDAO = go.AddComponent<GameObjectDAO> ();
		goDAO.Log = this.Log;
		return goDAO;
	} 
}

public class GameObjectDAO : MonoBehaviour {
	public bool Log;

	public IEnumerator Save (Task task, Action action) {
		yield return new WaitForSeconds (0.1f);
		if (Log) Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "Save", "WaitForSeconds (0.1f)"));

		if (task.IsFaulted) {
			ExceptionCallback (task.Exception);
		} else if (task.IsCompleted) {
			action ();
			if (Log) Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "Save", "task.IsCompleted"));
			GameObject.Destroy (gameObject);
		} else {
			yield return StartCoroutine (Save (task, action));
		}
	}

	public IEnumerator Load <T> (Task<T> task, Action<T> action) where T : ParseObject {
		yield return new WaitForSeconds (0.1f);
		if (Log) Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "Load", "WaitForSeconds (0.1f)"));

		if (task.IsFaulted) {
			ExceptionCallback (task.Exception);
		} else if (task.IsCompleted) {
			action (task.Result);
			if (Log) Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "Load", "task.Result = " + task.Result));
			GameObject.Destroy (gameObject);
		}else {
			yield return StartCoroutine (Load (task, action));
		}
	}

	public IEnumerator Load <T> (Task<IEnumerable<T>> task, Action<List<T>> action) where T : ParseObject {
		yield return new WaitForSeconds (0.1f);
		if (Log) Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "Load", "WaitForSeconds (0.1f)"));

		if (task.IsFaulted) {
			ExceptionCallback (task.Exception);
		} else if (task.IsCompleted) {
			List<T> list = new List<T> ();
			foreach (T t in task.Result) {
				list.Add (t);
			}
			action (list);
			if (Log) {
				if (list != null && list.Count > 0) {
					if (list.Count == 1) Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "Load", "list[0] = " + list[0]));
					if (list.Count > 1)  Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "Load", "list.Count = " + list.Count));
				} else {
					Debug.Log (string.Format ("[{0}.{1}] {2}", this.GetType (), "Load", "list.Count = 0"));
				}
			}
			GameObject.Destroy (gameObject);
		}else {
			yield return StartCoroutine (Load (task, action));
		}
	}

	private void ExceptionCallback (Exception e) {
		LoadingSystem.Instance.CloseLoading();
		Debug.LogException (e);
		GameObject.Destroy (gameObject);
	}
}
