﻿using UnityEngine;
using System.Collections;
using Parse;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class CartaDAO : ParseDAO<Carta> {
	public CartaDAO () {
		ParseObject.RegisterSubclass<Carta>();
	}

	public CartaDAO (bool log) {
		ParseObject.RegisterSubclass<Carta>();
		base.Log = log;
	}
}
