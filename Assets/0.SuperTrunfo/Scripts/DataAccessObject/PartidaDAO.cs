﻿using UnityEngine;
using System.Collections;
using Parse;
using System;
using System.Collections.Generic;

public class PartidaDAO : ParseDAO<Partida> {

	public PartidaDAO () {
		ParseObject.RegisterSubclass<Partida>();
	}

	public PartidaDAO (bool log) {
		ParseObject.RegisterSubclass<Partida>();
		base.Log = log;
	}

	public IEnumerator LoadByJogador (Jogador jogador, Action<List<Partida>> action) {
		var q1 = new ParseQuery<Partida> ();
		var q2 = new ParseQuery<Partida> ();

		var task = new ParseQuery<Partida> ().Or(q1.WhereEqualTo ("Jogador", jogador), q2.WhereEqualTo ("Adversario", jogador)).FindAsync ();
		
		return CreateGameObject ().Load (task, action);
	}
}
