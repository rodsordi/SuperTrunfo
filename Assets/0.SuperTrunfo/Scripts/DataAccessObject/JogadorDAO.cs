﻿using UnityEngine;
using System.Collections;
using Parse;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class JogadorDAO : ParseDAO<Jogador> {
	public JogadorDAO () {
		ParseObject.RegisterSubclass<Jogador>();
	}

	public JogadorDAO (bool log) {
		ParseObject.RegisterSubclass<Jogador>();
		base.Log = log;
	}

	public IEnumerator LoadByFacebookId (string FaceBookId, Action<List<Jogador>> action) {
		var query = new ParseQuery<Jogador> ();
		var task = query.WhereEqualTo ("FaceBookId", FaceBookId).FindAsync ();

		return CreateGameObject ().Load (task, action);
	}

	public IEnumerator LoadByNome (string FacebookFullName, Action<List<Jogador>> action) {
		var query = new ParseQuery<Jogador> ();
		var task = query.WhereEqualTo ("Nome", FacebookFullName).WhereEqualTo ("FaceBookId", null).FindAsync ();
		
		return CreateGameObject ().Load (task, action);
	}
}
