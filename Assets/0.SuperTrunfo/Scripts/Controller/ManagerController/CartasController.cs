﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CartasController : MonoBehaviour {

	public Rect NomeLabel;
	public Rect NomeField;
	
	public Rect Categoria1Label;
	public Rect Categoria2Label;
	public Rect Categoria3Label;
	public Rect Categoria4Label;
	
	public Rect Categoria1Field;
	public Rect Categoria2Field;
	public Rect Categoria3Field;
	public Rect Categoria4Field;
	
	public Rect Add;
	public Rect Cancel;
	
	private string NomeCarro = "Nome do Carro";
	//private string Categoria1 = "10";
	//private string Categoria2 = "7";
	//private string Categoria3 = "5";
	//private string Categoria4 = "3";
	
	void Start () {
		NomeLabel = new Rect (25, 25, 150, 25);
		NomeField = new Rect (150, 25, 150, 25);
		
		Categoria1Label = new Rect (25, 75, 150, 25);
		Categoria2Label = new Rect (25, 125, 150, 25);
		Categoria3Label = new Rect (25, 175, 150, 25);
		Categoria4Label = new Rect (25, 225, 150, 25);
		
		Categoria1Field = new Rect (150, 75, 150, 25);
		Categoria2Field = new Rect (150, 125, 150, 25);
		Categoria3Field = new Rect (150, 175, 150, 25);
		Categoria4Field = new Rect (150, 225, 150, 25);
		
		Add = new Rect (25, 350, 100, 25);
		Cancel = new Rect (135, 350, 100, 25);
	}
	
	void OnGUI () {
		for (int i = 0; i < 10; i++) {
			GUI.Label (new Rect (25, i * 25, 150, 25), "Nome do Carro");
			NomeCarro = GUI.TextField (new Rect (150, i * 25, 150, 25), NomeCarro);
		}

		if (GUI.Button (Add, "Novo")) {
			Application.LoadLevel("Carta");
		}
		
		if (GUI.Button (Cancel, "Cancel")) {
			Application.LoadLevel("");
		}
	}
}
