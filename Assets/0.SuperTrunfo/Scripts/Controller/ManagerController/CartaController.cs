﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CartaController : MonoBehaviour {

	public Rect NomeLabel;
	public Rect NomeField;

	public Rect Categoria1Label;
	public Rect Categoria2Label;
	public Rect Categoria3Label;
	public Rect Categoria4Label;

	public Rect Categoria1Field;
	public Rect Categoria2Field;
	public Rect Categoria3Field;
	public Rect Categoria4Field;

	public Rect Add;
	public Rect Cancel;
	
	private string NomeCarro = "Nome do Carro";
	private string Categoria1 = "10";
	private string Categoria2 = "7";
	private string Categoria3 = "5";
	private string Categoria4 = "3";

	void Start () {
		NomeLabel = new Rect (25, 25, 150, 25);
		NomeField = new Rect (150, 25, 150, 25);

		Categoria1Label = new Rect (25, 75, 150, 25);
		Categoria2Label = new Rect (25, 125, 150, 25);
		Categoria3Label = new Rect (25, 175, 150, 25);
		Categoria4Label = new Rect (25, 225, 150, 25);

		Categoria1Field = new Rect (150, 75, 150, 25);
		Categoria2Field = new Rect (150, 125, 150, 25);
		Categoria3Field = new Rect (150, 175, 150, 25);
		Categoria4Field = new Rect (150, 225, 150, 25);

		Add = new Rect (25, 350, 100, 25);
		Cancel = new Rect (135, 350, 100, 25);
	}
	
	void OnGUI () {
		GUI.Label (NomeLabel, "Nome do Carro");
		NomeCarro = GUI.TextField (NomeField, NomeCarro);

		GUI.Label (Categoria1Label, "Categoria1Label");
		GUI.Label (Categoria2Label, "Categoria2Label");
		GUI.Label (Categoria3Label, "Categoria3Label");
		GUI.Label (Categoria4Label, "Categoria4Label");
		
		Categoria1 = GUI.TextField (Categoria1Field, Categoria1);
		Categoria2 = GUI.TextField (Categoria2Field, Categoria2);
		Categoria3 = GUI.TextField (Categoria3Field, Categoria3);
		Categoria4 = GUI.TextField (Categoria4Field, Categoria4);

		
		if (GUI.Button (Add, "Add")) {
			Application.LoadLevel("Cartas");
		}
		
		if (GUI.Button (Cancel, "Cancel")) {
			Application.LoadLevel("Cartas");
		}
	}
}
