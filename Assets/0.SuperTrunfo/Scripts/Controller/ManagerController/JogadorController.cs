﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class JogadorController : MonoBehaviour {

	public Rect Label;
	public Rect Field;
	public Rect Add;
	public Rect Cancel;
	
	private string textFieldString = "Nome do Jogador";
	
	void Start () {
		Label = new Rect (25, 25, 150, 25);
		Field = new Rect (150, 25, 150, 25);
		Add = new Rect (25, 350, 100, 25);
		Cancel = new Rect (135, 350, 100, 25);
	}
	
	void OnGUI () {
		GUI.Label (Label, "Nome do Jogador");
		textFieldString = GUI.TextField (Field, textFieldString);

		for (int i = 2; i < 5; i++) {
			if (GUI.Button (new Rect (25, i * 25, 150, 25), "Carta")) {

			}
		}

		for (int i = 2; i < 5; i++) {
			if (GUI.Button (new Rect (175, i * 25, 150, 25), "Carta")) {
				
			}
		}

		for (int i = 2; i < 5; i++) {
			if (GUI.Button (new Rect (350, i * 25, 150, 25), "Amigo")) {
				
			}
		}
		
		for (int i = 2; i < 5; i++) {
			if (GUI.Button (new Rect (500, i * 25, 150, 25), "Amigo")) {
				
			}
		}
		
		if (GUI.Button (Add, "Add")) {
			Application.LoadLevel("Jogadores");
		}
		
		if (GUI.Button (Cancel, "Cancel")) {
			Application.LoadLevel("Jogadores");
		}
	}
}
