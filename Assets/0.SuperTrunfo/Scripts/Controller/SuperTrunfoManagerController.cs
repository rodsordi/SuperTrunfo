﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SuperTrunfoManagerController : MonoBehaviour {

	private int toolbarInt = 0;
	private string[] toolbarStrings = {"Toolbar1", "Toolbar2", "Toolbar3"};
	
	void OnGUI () {
		toolbarInt = GUI.Toolbar (new Rect (25, 25, 250, 30), toolbarInt, toolbarStrings);
	}
}
