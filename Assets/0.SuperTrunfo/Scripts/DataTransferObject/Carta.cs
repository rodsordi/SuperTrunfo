﻿using UnityEngine;
using System.Collections;
using System;
using Parse;
using System.Collections.Generic;

[ParseClassName("Carta")]
public class Carta : ParseObject {

	[ParseFieldName("NomeCarro")]
	public string NomeCarro { 
		get { return GetProperty<string>("NomeCarro");} 
		set { SetProperty<string>(value, "NomeCarro");}
	}

	[ParseFieldName("Potencia")]
	public int Potencia { 
		get { return GetProperty<int>("Potencia");} 
		set { SetProperty<int>(value, "Potencia");}
	}
	[ParseFieldName("Torque")]
	public int Torque { 
		get { return GetProperty<int>("Torque");} 
		set { SetProperty<int>(value, "Torque");}
	}
	[ParseFieldName("ConsumoUrbano")]
	public int ConsumoUrbano { 
		get { return GetProperty<int>("ConsumoUrbano");} 
		set { SetProperty<int>(value, "ConsumoUrbano");}
	}
	[ParseFieldName("Ruido")]
	public int Ruido { 
		get { return GetProperty<int>("Ruido");} 
		set { SetProperty<int>(value, "Ruido");}
	}
	[ParseFieldName("Aceleracao")]
	public int Aceleracao { 
		get { return GetProperty<int>("Aceleracao");} 
		set { SetProperty<int>(value, "Aceleracao");}
	}
	[ParseFieldName("Frenagem")]
	public int Frenagem { 
		get { return GetProperty<int>("Frenagem");} 
		set { SetProperty<int>(value, "Frenagem");}
	}
	[ParseFieldName("Retomada")]
	public int Retomada { 
		get { return GetProperty<int>("Retomada");} 
		set { SetProperty<int>(value, "Retomada");}
	}
	[ParseFieldName("PortaMalas")]
	public int PortaMalas { 
		get { return GetProperty<int>("PortaMalas");} 
		set { SetProperty<int>(value, "PortaMalas");}
	}

	[ParseFieldName("Categoria")]
	public string Categoria { 
		get { return GetProperty<string>("Categoria");} 
		set { SetProperty<string>(value, "Categoria");}
	}

	[ParseFieldName("Picture")]
	public ParseFile Picture { 
		get { return GetProperty<ParseFile>("Picture");} 
		set { SetProperty<ParseFile>(value, "Picture");}
	}

	public Carta (){}

	public Carta (string nomeCarro, int potencia, int torque, int consumoUrbano, int ruido, int aceleracao, int frenagem, int retomada, int portaMalas, string categoria, ParseFile picture)
	{
		this.NomeCarro = nomeCarro;
		this.Potencia = potencia;
		this.Torque = torque;
		this.ConsumoUrbano = consumoUrbano;
		this.Ruido = ruido;
		this.Aceleracao = aceleracao;
		this.Frenagem = frenagem;
		this.Retomada = retomada;
		this.PortaMalas = portaMalas;
		this.Categoria = categoria;
		this.Picture = picture;
	}
	

	/*public Carta (string nomeCarro, int categoria1, int categoria2, int categoria3, int categoria4)
	{
		this.NomeCarro = nomeCarro;
		this.Categoria1 = categoria1;
		this.Categoria2 = categoria2;
		this.Categoria3 = categoria3;
		this.Categoria4 = categoria4;
	}

	public Carta (ParseObject po) {
		this.ObjectId = po.ObjectId;

		if(po.ContainsKey("NomeCarro")) this.NomeCarro = po.Get<string> ("NomeCarro");
		if(po.ContainsKey("Categoria1")) this.Categoria1 = po.Get<int> ("Categoria1");
		if(po.ContainsKey("Categoria2")) this.Categoria2 = po.Get<int> ("Categoria2");
		if(po.ContainsKey("Categoria3")) this.Categoria3 = po.Get<int> ("Categoria3");
		if(po.ContainsKey("Categoria4")) this.Categoria4 = po.Get<int> ("Categoria4");

		if(po.ContainsKey("Picture")) this.Picture = po.Get<ParseFile>("Picture");
	}*/

	public override string ToString ()
	{
		return string.Format (ObjectId + " [Carta: NomeCarro={0}, Potencia={1}, Torque={2}, ConsumoUrbano={3}, Ruido={4}, Aceleracao={5}, Frenagem={6}, Retomada={7}, PortaMalas={8}, Categoria={9}]", NomeCarro, Potencia, Torque, ConsumoUrbano, Ruido, Aceleracao, Frenagem, Retomada, PortaMalas, Categoria);
	}
	
	
}