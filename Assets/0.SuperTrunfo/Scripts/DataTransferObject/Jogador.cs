﻿using UnityEngine;
using System.Collections;
using System;
using Parse;
using System.Collections.Generic;

[ParseClassName("Jogador")]
public class Jogador : ParseObject {

	[ParseFieldName("FaceBookId")]
	public string FaceBookId { 
		get { return GetProperty<string>("FaceBookId");} 
		set { SetProperty<string>(value, "FaceBookId");}
	}

	[ParseFieldName("Nome")]
	public string Nome { 
		get { return GetProperty<string>("Nome");} 
		set { SetProperty<string>(value, "Nome");}
	}

	[ParseFieldName("FotoURL")]
	public string FotoURL { 
		get { return GetProperty<string>("FotoURL");} 
		set { SetProperty<string>(value, "FotoURL");}
	}

	[ParseFieldName("Vitorias")]
	public int Vitorias { 
		get { return GetProperty<int>("Vitorias");} 
		set { SetProperty<int>(value, "Vitorias");}
	}

	public Jogador (){}

	public override string ToString ()
	{
		return string.Format (ObjectId + " [Jogador: FaceBookId={0}, Nome={1}]", FaceBookId, Nome);
	}
	
}