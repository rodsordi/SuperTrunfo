﻿using UnityEngine;
using System.Collections;
using System;
using Parse;
using System.Collections.Generic;

[ParseClassName("Partida")]
public class Partida : ParseObject {

	public static readonly int STATUS_ABERTO = 0;
	public static readonly int STATUS_JOGANDO = 1;
	public static readonly int STATUS_FECHADO = 2;

	public static readonly string TURNO_JOGADOR = "Jogador";
	public static readonly string TURNO_ADVERSARIO = "Adversario";

	[ParseFieldName("Jogador")]
	public Jogador Jogador { 
		get { return GetProperty<Jogador>("Jogador");} 
		set { SetProperty <Jogador> (ParseObject.CreateWithoutData<Jogador>(value.ObjectId), "Jogador");}
	}
	[ParseFieldName("Adversario")]
	public Jogador Adversario { 
		get { return GetProperty<Jogador>("Adversario");} 
		set { SetProperty <Jogador> (ParseObject.CreateWithoutData<Jogador>(value.ObjectId), "Adversario");}
	}



	[ParseFieldName("CartasJogador")]
	public List<object> CartasJogador { 
		get { return GetProperty<List<object>>("CartasJogador");}
		set { SetProperty<List<object>>(value, "CartasJogador");}
	}

	[ParseFieldName("CartasAdversario")]
	public List<object> CartasAdversario { 
		get { return GetProperty<List<object>>("CartasAdversario");}
		set { SetProperty<List<object>>(value, "CartasAdversario");}
	}

	[ParseFieldName("MaoJogador")]
	public List<object> MaoJogador { 
		get { return GetProperty<List<object>>("MaoJogador");}
		set { SetProperty<List<object>>(value, "MaoJogador");}
	}
	
	[ParseFieldName("MaoAdversario")]
	public List<object> MaoAdversario { 
		get { return GetProperty<List<object>>("MaoAdversario");}
		set { SetProperty<List<object>>(value, "MaoAdversario");}
	}

	[ParseFieldName("MaoJogadorAnterior")]
	public List<object> MaoJogadorAnterior { 
		get { return GetProperty<List<object>>("MaoJogadorAnterior");}
		set { SetProperty<List<object>>(value, "MaoJogadorAnterior");}
	}
	
	[ParseFieldName("MaoAdversarioAnterior")]
	public List<object> MaoAdversarioAnterior { 
		get { return GetProperty<List<object>>("MaoAdversarioAnterior");}
		set { SetProperty<List<object>>(value, "MaoAdversarioAnterior");}
	}

	[ParseFieldName("CategoriaCarta1")]
	public int CategoriaCarta1 { 
		get { return GetProperty<int>("CategoriaCarta1");} 
		set { SetProperty<int>(value, "CategoriaCarta1");}
	}
	[ParseFieldName("CategoriaCarta2")]
	public int CategoriaCarta2 { 
		get { return GetProperty<int>("CategoriaCarta2");} 
		set { SetProperty<int>(value, "CategoriaCarta2");}
	}
	[ParseFieldName("CategoriaCarta3")]
	public int CategoriaCarta3 { 
		get { return GetProperty<int>("CategoriaCarta3");} 
		set { SetProperty<int>(value, "CategoriaCarta3");}
	}
	[ParseFieldName("CategoriaCarta4")]
	public int CategoriaCarta4 { 
		get { return GetProperty<int>("CategoriaCarta4");} 
		set { SetProperty<int>(value, "CategoriaCarta4");}
	}

	[ParseFieldName("CategoriaOld1")]
	public int CategoriaOld1 { 
		get { return GetProperty<int>("CategoriaOld1");} 
		set { SetProperty<int>(value, "CategoriaOld1");}
	}
	[ParseFieldName("CategoriaOld2")]
	public int CategoriaOld2 { 
		get { return GetProperty<int>("CategoriaOld2");} 
		set { SetProperty<int>(value, "CategoriaOld2");}
	}
	[ParseFieldName("CategoriaOld3")]
	public int CategoriaOld3 { 
		get { return GetProperty<int>("CategoriaOld3");} 
		set { SetProperty<int>(value, "CategoriaOld3");}
	}
	[ParseFieldName("CategoriaOld4")]
	public int CategoriaOld4 { 
		get { return GetProperty<int>("CategoriaOld4");} 
		set { SetProperty<int>(value, "CategoriaOld4");}
	}

	[ParseFieldName("Rodada")]
	public int Rodada { 
		get { return GetProperty<int>("Rodada");} 
		set { SetProperty<int>(value, "Rodada");}
	}

	[ParseFieldName("Turno")]
	public string Turno { 
		get { return GetProperty<string>("Turno");} 
		set { SetProperty<string>(value, "Turno");}
	}

	[ParseFieldName("StatusPartida")]
	public int StatusPartida { 
		get { return GetProperty<int>("StatusPartida");} 
		set { SetProperty<int>(value, "StatusPartida");}
	}

	[ParseFieldName("DatePartida")]
	public DateTime DatePartida { 
		get { return GetProperty<DateTime>("DatePartida");} 
		set { SetProperty<DateTime>(value, "DatePartida");}
	}

	public Partida () {}


}
