﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DeveloperMode : MonoBehaviour {
	public int boxX, boxY, boxHeight, boxWidth;
	public int button1X, button1Y, button2X, button2Y;
	public int buttonWidth, buttonHeight;

	void OnGUI () {
		GUI.Box(new Rect(boxX,boxY,boxHeight,boxWidth), "Loader Menu");

		if(GUI.Button(new Rect(button1X,button1Y,buttonWidth,buttonHeight), "Level 1")) {
			//Application.LoadLevel(1);
		}

		if(GUI.Button(new Rect(button2X,button2Y,buttonWidth,buttonHeight), "Level 2")) {
			//Application.LoadLevel(2);
		}


	}

}
