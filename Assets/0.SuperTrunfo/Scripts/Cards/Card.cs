﻿using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour
{
	
		public static string cardName = "Porche";
		public static string cardGroup = "A";
		public static string cardNumber = "01";
		public static string cardPhoto = "01A.jpg";
		public static string cardCategory01 = "Potência";
		public static string cardCategory02 = "Torque";
		public static string cardCategory03 = "Cons. urb";
		public static string cardCategory04 = "Ruído p.m";
		public static string cardCategory05 = "Aceleração";
		public static string cardCategory06 = "Fren. 120-0";
		public static string cardCategory07 = "Retorm 40-80";
		public static string cardCategory08 = "Porta-malas";
		public static int cardCategory01_value = 100;
		public static int cardCategory02_value = 200;
		public static int cardCategory03_value = 300;
		public static int cardCategory04_value = 400;
		public static int cardCategory05_value = 500;
		public static int cardCategory06_value = 600;
		public static int cardCategory07_value = 700;
		public static int cardCategory08_value = 800;
		public static Card instance;

		public static Card Instance {
				get {
						if (instance == null) {
								var gB = new GameObject ("GameCards");
								instance = gB.AddComponent<Card> ();
						}
						return instance;
				}
		}
}
