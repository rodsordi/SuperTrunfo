﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
		public static GameController _singleton;
	
		public static Jogador Jogador;
		public static List<Partida> Partidas;
		public static Partida Partida;

		public static GameController GetInstance ()
		{
				if (_singleton == null) {
						Instantiate (Resources.Load ("GameController"));
				}
				return _singleton;
		}
}
